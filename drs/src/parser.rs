//! Handles the nom parsing code for each of the different DRS specifications and puts commonly used functions in
//! [`common`].
//!
//! Each of the functions this module provides expect to receive just the portion of the path relevant to their
//! respective DRS specification.
//!
//! Documentation about the different specifications can be found in the respective `drs::<spec>` modules as those are
//! public facing and are not be replicated within their `parser` counterparts.

pub(crate) mod cmip5;
mod cmip6;
pub(crate) mod common;
mod cordex;

pub(crate) use cmip5::{parse_cmor, parse_esgf};
pub(crate) use cmip6::parse_cmip6;
pub(crate) use cordex::parse_cordex;
