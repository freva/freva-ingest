use nom::{
    branch::alt,
    bytes::complete::{tag, take_while, take_while_m_n},
    character::complete::{alpha1, u32 as nom_u32},
    combinator::{eof, opt, recognize, verify},
    sequence::{preceded, terminated, tuple},
    IResult,
};

use crate::{
    cmip6::{MemberId, PathMetadata, VariantLabel, MIP_ERA},
    parser::common::{name_segment, parse_temporal_subset, parse_version, path_segment},
};

use super::common::{name_sep, path_sep, word};

/// Parses a string as a CMIP6 path. This assumes that the string it's given will be just the portion of a path that
/// is relevant to CMIP6 meaning any preceding directories have already been removed.
pub(crate) fn parse_cmip6(path: &str) -> IResult<&str, PathMetadata> {
    /*
    <mip_era>/<activity_id>/<institution_id>/<source_id>/<experiment_id>/
    <member_id>/<table_id>/<variable_id>/<grid_label>/<version>/<filename>
    */
    let (i, _mip_era) = terminated(parse_mip_era, path_sep)(path)?;
    let (i, activity_id) = path_segment(i)?;
    let (i, institution_id) = path_segment(i)?;
    let (i, source_id) = path_segment(i)?;
    let (i, experiment_id) = path_segment(i)?;
    let (i, member_id) = terminated(parse_member_id, path_sep)(i)?;
    let (i, table_id) = path_segment(i)?; // this may be enumable
    let (i, variable_id) = path_segment(i)?;
    let (i, grid_label) = terminated(parse_grid_label, path_sep)(i)?;
    let (i, version) = terminated(parse_version, path_sep)(i)?;
    /*
    <variable_id>_<table_id>_<source_id>_<experiment_id>_<member_id>_
    <grid_label>[_<time_range>].nc
    */
    let (i, _variable_id) = name_segment(i)?;
    let (i, _table_id) = name_segment(i)?;
    let (i, _source_id) = name_segment(i)?;
    let (i, _experiment_id) = name_segment(i)?;
    let (i, _member_id) = terminated(parse_member_id, name_sep)(i)?;
    let (i, _grid_label) = parse_grid_label(i)?;
    let (i, time_range) = opt(parse_temporal_subset)(i)?;

    let (i, _) = tag(".nc")(i)?;
    let (i, _) = eof(i)?;

    Ok((
        i,
        PathMetadata {
            activity_id,
            institution_id,
            source_id,
            experiment_id,
            member_id,
            table_id,
            variable_id,
            grid_label,
            version,
            time_range,
        },
    ))
}

fn parse_mip_era(i: &str) -> IResult<&str, &str> {
    // get the segment and check that it is cmip6 which is required by the spec
    verify(word, |w: &str| w == MIP_ERA)(i)
}

fn parse_member_id(i: &str) -> IResult<&str, MemberId> {
    let (i, sub_experiment_id) = opt(terminated(parse_sub_experiment_id, tag("-")))(i)?;
    let (i, variant_label) = parse_variant_label(i)?;

    Ok((
        i,
        MemberId {
            sub_experiment_id,
            variant_label,
        },
    ))
}

fn parse_sub_experiment_id(i: &str) -> IResult<&str, &str> {
    recognize(alt((tag("none"), parse_initialization_year)))(i)
}

fn parse_initialization_year(i: &str) -> IResult<&str, &str> {
    let (i, _) = tag("s")(i)?;
    take_while_m_n(4, 4, |c: char| c.is_numeric())(i)
}

fn parse_variant_label(i: &str) -> IResult<&str, VariantLabel> {
    let (i, realization) = preceded(tag("r"), nom_u32)(i)?;
    let (i, initialization) = preceded(tag("i"), nom_u32)(i)?;
    let (i, physics) = preceded(tag("p"), nom_u32)(i)?;
    let (i, forcing) = preceded(tag("f"), nom_u32)(i)?;

    Ok((
        i,
        VariantLabel {
            realization,
            initialization,
            physics,
            forcing,
        },
    ))
}

fn parse_grid_label(i: &str) -> IResult<&str, &str> {
    recognize(tuple((
        tag("g"),
        alt((tag("m"), parse_native_grid, parse_regridded)),
    )))(i)
}

fn parse_native_grid(i: &str) -> IResult<&str, &str> {
    // technically allows for multiple characters following despite that not being possible in the CV
    recognize(tuple((tag("n"), opt(alpha1))))(i)
}

fn parse_regridded(i: &str) -> IResult<&str, &str> {
    recognize(tuple((tag("r"), take_while(|c: char| c.is_alphanumeric()))))(i)
}

#[cfg(test)]
mod tests {
    use crate::cmip5::{PartialDateTime, TemporalSubset};

    use super::*;

    #[test]
    fn test_grid_label() {
        let cases = vec!["gr", "gm", "gn", "gna", "gng", "gr1", "gr1z", "grz"];
        for (i, c) in cases.into_iter().enumerate() {
            let res = parse_grid_label(c);
            assert!(res.is_ok(), "error parsing grid label on case {}", i);
            let (remaining, output) = res.unwrap();
            assert_eq!(
                remaining, "",
                "expected no remaining data to parse on case {}",
                i
            );
            assert_eq!(output, c, "output did not match expected on case {}", i);
        }
    }

    #[test]
    fn test_full_paths() {
        let cases = full_path_data();
        for (i, (input, expected)) in cases.into_iter().enumerate() {
            let (remaining, output) = parse_cmip6(input).expect("error parsing path");
            assert_eq!(
                remaining, "",
                "expected no remaining data to parse on case {}",
                i
            );
            assert_eq!(
                expected, output,
                "output did not match expected on case {}",
                i
            );
        }
    }

    fn full_path_data() -> Vec<(&'static str, PathMetadata<'static>)> {
        vec![
            (
                concat!(
                    "CMIP6/CMIP/HAMMOZ-Consortium/MPI-ESM-1-2-HAM/1pctCO2/r1i1p1f1/Lmon/tsl/gn/v20190628/",
                    "tsl_Lmon_MPI-ESM-1-2-HAM_1pctCO2_r1i1p1f1_gn_191001-192912.nc"
                ),
                PathMetadata {
                    activity_id: "CMIP",
                    institution_id: "HAMMOZ-Consortium",
                    source_id: "MPI-ESM-1-2-HAM",
                    experiment_id: "1pctCO2",
                    member_id: MemberId {
                        sub_experiment_id: None,
                        variant_label: VariantLabel {
                            realization: 1,
                            initialization: 1,
                            physics: 1,
                            forcing: 1
                        }
                    },
                    table_id: "Lmon",
                    variable_id: "tsl",
                    grid_label: "gn",
                    version: "v20190628",
                    time_range: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1910, 1),
                        end: PartialDateTime::from_ym(1929, 12),
                        suffix: None,
                    }),
                },
            ),
            (
                concat!(
                    "CMIP6/AerChemMIP/BCC/BCC-ESM1/hist-piAer/r1i1p1f1/AERmon/c2h6/gn/v20200511/",
                    "c2h6_AERmon_BCC-ESM1_hist-piAer_r1i1p1f1_gn_185001-201412.nc"
                ),
                PathMetadata {
                    activity_id: "AerChemMIP",
                    institution_id: "BCC",
                    source_id: "BCC-ESM1",
                    experiment_id: "hist-piAer",
                    member_id: MemberId {
                        sub_experiment_id: None,
                        variant_label: VariantLabel {
                            realization: 1,
                            initialization: 1,
                            physics: 1,
                            forcing: 1
                        }
                    },
                    table_id: "AERmon",
                    variable_id: "c2h6",
                    grid_label: "gn",
                    version: "v20200511",
                    time_range: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(2014, 12),
                        suffix: None,
                    }),
                }
            ),
            (
                concat!(
                    "CMIP6/GeoMIP/CCCma/CanESM5/G1/r3i1p2f1/Ofx/areacello/gn/v20190429/",
                    "areacello_Ofx_CanESM5_G1_r3i1p2f1_gn.nc"
                ),
                PathMetadata {
                    activity_id: "GeoMIP",
                    institution_id: "CCCma",
                    source_id: "CanESM5",
                    experiment_id: "G1",
                    member_id: MemberId {
                        sub_experiment_id: None,
                        variant_label: VariantLabel {
                            realization: 3,
                            initialization: 1,
                            physics: 2,
                            forcing: 1
                        }
                    },
                    table_id: "Ofx",
                    variable_id: "areacello",
                    grid_label: "gn",
                    version: "v20190429",
                    time_range: None,
                }
            ),
            (
                concat!(
                    "CMIP6/HighResMIP/NOAA-GFDL/GFDL-CM4C192/highresSST-future/r1i1p1f1/6hrPlevPt/psl/gr3/v20180701/",
                    "psl_6hrPlevPt_GFDL-CM4C192_highresSST-future_r1i1p1f1_gr3_203501010600-205101010000.nc"
                ),
                PathMetadata {
                    activity_id: "HighResMIP",
                    institution_id: "NOAA-GFDL",
                    source_id: "GFDL-CM4C192",
                    experiment_id: "highresSST-future",
                    member_id: MemberId {
                        sub_experiment_id: None,
                        variant_label: VariantLabel {
                            realization: 1,
                            initialization: 1,
                            physics: 1,
                            forcing: 1
                        }
                    },
                    table_id: "6hrPlevPt",
                    variable_id: "psl",
                    grid_label: "gr3",
                    version: "v20180701",
                    time_range: Some(TemporalSubset {
                        start: PartialDateTime::from_ymd_hm(2035, 1, 1, 6, 0),
                        end: PartialDateTime::from_ymd_hm(2051, 1, 1, 0, 0),
                        suffix: None,
                    }),
                }
            ),
            (
                concat!(
                    "CMIP6/ScenarioMIP/UA/MCM-UA-1-0/ssp370/r1i1p1f2/SImon/sithick/gn/v20190731/",
                    "sithick_SImon_MCM-UA-1-0_ssp370_r1i1p1f2_gn_201501-210012.nc"
                ),
                PathMetadata {
                    activity_id: "ScenarioMIP",
                    institution_id: "UA",
                    source_id: "MCM-UA-1-0",
                    experiment_id: "ssp370",
                    member_id: MemberId {
                        sub_experiment_id: None,
                        variant_label: VariantLabel {
                            realization: 1,
                            initialization: 1,
                            physics: 1,
                            forcing: 2
                        }
                    },
                    table_id: "SImon",
                    variable_id: "sithick",
                    grid_label: "gn",
                    version: "v20190731",
                    time_range: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(2015, 01),
                        end: PartialDateTime::from_ym(2100, 12),
                        suffix: None,
                    }),
                }
            ),
            (
                concat!(
                    "CMIP6/CDRMIP/NCAR/CESM2/1pctCO2-cdr/r1i1p1f1/Omon/intpn2/gr/v20190613/",
                    "intpn2_Omon_CESM2_1pctCO2-cdr_r1i1p1f1_gr_005001-009912.nc"
                ),
                PathMetadata {
                    activity_id: "CDRMIP",
                    institution_id: "NCAR",
                    source_id: "CESM2",
                    experiment_id: "1pctCO2-cdr",
                    member_id: MemberId {
                        sub_experiment_id: None,
                        variant_label: VariantLabel {
                            realization: 1,
                            initialization: 1,
                            physics: 1,
                            forcing: 1
                        }
                    },
                    table_id: "Omon",
                    variable_id: "intpn2",
                    grid_label: "gr",
                    version: "v20190613",
                    time_range: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(50, 1),
                        end: PartialDateTime::from_ym(99, 12),
                        suffix: None,
                    }),
                }
            )
        ]
    }
}
