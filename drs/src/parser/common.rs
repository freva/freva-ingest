use nom::{
    bytes::complete::{tag, take_while1, take_while_m_n},
    character::complete::digit1,
    combinator::{map_res, opt, recognize},
    sequence::{terminated, tuple},
    IResult,
};

use crate::cmip5::{PartialDateTime, TemporalSubset};

pub(super) fn parse_version(i: &str) -> IResult<&str, &str> {
    recognize(tuple((tag("v"), digit1)))(i)
}

pub(super) fn parse_temporal_subset(i: &str) -> IResult<&str, TemporalSubset> {
    let (i, _) = name_sep(i)?; // drop the name separator
    let (i, (start, _, end)) = tuple((parse_time, tag("-"), parse_time))(i)?;
    let (i, suffix) = opt(word)(i)?;

    Ok((i, TemporalSubset { start, end, suffix }))
}

pub(crate) fn parse_time(i: &str) -> IResult<&str, PartialDateTime> {
    let (i, year) = parse_n_i32(4)(i)?;
    let (i, month) = opt(parse_n_u32(2))(i)?;
    let (i, day) = opt(parse_n_u32(2))(i)?;
    let (i, hour) = opt(parse_n_u32(2))(i)?;
    let (i, minute) = opt(parse_n_u32(2))(i)?;
    let (i, second) = opt(parse_n_u32(2))(i)?;

    Ok((
        i,
        PartialDateTime {
            year,
            month,
            day,
            hour,
            minute,
            second,
        },
    ))
}

/// Parses a segment of the path when no special verification is required.
/// This is a shorthand for consuming both a section of the path and following `/` while discarding the `/`.
///
/// # Examples
/// ```ignore
/// assert_eq!(segment("foo/"), Ok(("", "foo")));
/// ```
pub(super) fn path_segment(i: &str) -> IResult<&str, &str> {
    terminated(word, path_sep)(i)
}

pub(super) fn name_segment(i: &str) -> IResult<&str, &str> {
    terminated(word, name_sep)(i)
}

/// Parser for a mostly alphanumeric text segment.
/// Most DRS segments allow `a-zA-Z0-9\-` i.e. alphanumeric plus `-` so this parser will accept a full string of those.
pub(super) fn word(i: &str) -> IResult<&str, &str> {
    take_while1(|c: char| c.is_alphanumeric() || c == '-')(i)
}

/// Parser for all path separators.
///
/// Allows for multiple consecutive separators though such a path would likely be invalid or incorrect anyway.
pub(super) fn path_sep(i: &str) -> IResult<&str, &str> {
    take_while1(std::path::is_separator)(i)
}

/// Parser for the separator used within the filename.
pub(super) fn name_sep(i: &str) -> IResult<&str, &str> {
    tag("_")(i)
}

pub(super) fn parse_n_u32<'a>(n: usize) -> impl FnMut(&'a str) -> IResult<&'a str, u32> {
    map_res(
        take_while_m_n(n, n, |c: char| c.is_ascii_digit()),
        str::parse,
    )
}

pub(super) fn parse_n_i32<'a>(n: usize) -> impl FnMut(&'a str) -> IResult<&'a str, i32> {
    map_res(
        take_while_m_n(n, n, |c: char| c.is_ascii_digit()),
        str::parse,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_path_segment() {
        let i = "foo/bar/baz";
        let (i, foo) = path_segment(i).unwrap();
        assert_eq!("bar/baz", i);
        assert_eq!("foo", foo);
    }

    #[test]
    fn test_name_segment() {
        let i = "foo_bar_baz";
        let (i, foo) = name_segment(i).unwrap();
        assert_eq!("bar_baz", i);
        assert_eq!("foo", foo);
    }

    #[test]
    fn test_parse_time() {
        let cases = &[
            ("1901", (1901, None, None, None, None, None)),
            ("190102", (1901, Some(2), None, None, None, None)),
            ("19010203", (1901, Some(2), Some(3), None, None, None)),
            ("1901020304", (1901, Some(2), Some(3), Some(4), None, None)),
            (
                "190102030405",
                (1901, Some(2), Some(3), Some(4), Some(5), None),
            ),
            (
                "19010203040506",
                (1901, Some(2), Some(3), Some(4), Some(5), Some(6)),
            ),
            (
                "19011203040506",
                (1901, Some(12), Some(3), Some(4), Some(5), Some(6)),
            ),
            (
                "19011203040526",
                (1901, Some(12), Some(3), Some(4), Some(5), Some(26)),
            ),
        ];
        for (s, expected) in cases.iter() {
            let res = parse_time(s);
            assert_eq!(
                Ok((
                    "",
                    PartialDateTime {
                        year: expected.0,
                        month: expected.1,
                        day: expected.2,
                        hour: expected.3,
                        minute: expected.4,
                        second: expected.5,
                    }
                )),
                res,
                "input '{s}' did not give expected result '{expected:?}'"
            );
        }
    }
}
