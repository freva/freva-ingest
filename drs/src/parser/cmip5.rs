use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha1, digit1, u32 as nom_u32};
use nom::combinator::{eof, map_res, opt, recognize, verify};
use nom::sequence::{pair, preceded, terminated, tuple};
use nom::IResult;
use std::str::FromStr;

use crate::cmip5::{
    EnsembleMember, Frequency, GeographicalInfo, ModelingRealm, PathMetadata, Region,
    TemporalSubset,
};

use super::common::{
    name_segment, name_sep, parse_temporal_subset, parse_version, path_segment, path_sep, word,
};

/// Parses a string as a CMOR path. This assumes that the string it's given will be just the portion of a path that
/// is relevant to CMOR meaning any preceding directories have already been removed.
pub(crate) fn parse_cmor(path: &str) -> IResult<&str, PathMetadata> {
    /*
    <activity>/<product>/<institute>/<model>/<experiment>/
    <frequency>/<modeling_realm>/<variable_name>/<ensemble_member>
    */
    let (i, activity) = path_segment(path)?;
    let (i, product) = path_segment(i)?;
    let (i, institute) = path_segment(i)?;
    let (i, model) = path_segment(i)?;
    let (i, experiment) = path_segment(i)?;
    let (i, frequency) = terminated(parse_frequency, path_sep)(i)?;
    let (i, modeling_realm) = terminated(parse_modeling_realm, path_sep)(i)?;
    let (i, variable) = path_segment(i)?;
    let (i, ensemble) = terminated(parse_ensemble, path_sep)(i)?;
    /*
    Now we're in the filename portion
    <variable_name>_<mip_table>_<model>_<experiment>_
    <ensemble_member>[_<temporal_subset>][_<geographical_info>]
    */
    let (i, _variable) = name_segment(i)?;
    let (i, mip_table) = name_segment(i)?;
    let (i, _model) = name_segment(i)?;
    let (i, _experiment) = name_segment(i)?;
    let (i, _ensemble) = parse_ensemble(i)?;
    let (i, temporal_subset) = opt(parse_temporal_subset)(i)?;
    let (i, geographical_info) = opt(parse_geographical_info)(i)?;

    // The specification explicitly has DRS files ending with `.nc` so assuming they're netCDF files (or willing to make
    // themselves look like netCDF files), this verifies that.
    let (i, _) = tag(".nc")(i)?;
    // verify that we've reached the end of the input, if not, this file is not a valid cmor path
    let (i, _) = eof(i)?;

    Ok((
        i,
        PathMetadata {
            activity,
            product,
            institute,
            model,
            experiment,
            frequency,
            modeling_realm,
            variable,
            ensemble,
            mip_table,
            temporal_subset,
            geographical_info,
            version: None,
        },
    ))
}

/// Parses a string as an ESGF path. This assumes that the string it's given will be just the portion of a path that
/// is relevant to ESGF meaning any preceding directories have already been removed.
pub(crate) fn parse_esgf(path: &str) -> IResult<&str, PathMetadata> {
    /*
    <activity>/<product>/<institute>/<model>/<experiment>/<frequency>/
    <modeling_realm>/<mip_table>/<ensemble_member>/<version>/
    <variable_name>
    */
    let (i, activity) = path_segment(path)?;
    let (i, product) = path_segment(i)?;
    let (i, institute) = path_segment(i)?;
    let (i, model) = path_segment(i)?;
    let (i, experiment) = path_segment(i)?;
    let (i, frequency) = terminated(parse_frequency, path_sep)(i)?;
    let (i, modeling_realm) = terminated(parse_modeling_realm, path_sep)(i)?;
    let (i, mip_table) = path_segment(i)?;
    let (i, ensemble) = terminated(parse_ensemble, path_sep)(i)?;
    let (i, version) = terminated(parse_version, path_sep)(i)?;
    let (i, variable) = path_segment(i)?;

    let (i, (temporal_subset, geographical_info)) =
        alt((parse_esgf_gridspec_filename, parse_esgf_filename))(i)?;

    // The specification explicitly has DRS files ending with `.nc` so assuming they're netCDF files (or willing to make
    // themselves look like netCDF files), this verifies that.
    let (i, _) = tag(".nc")(i)?;
    // verify that we've reached the end of the input, if not, this file is not a valid cmor path
    let (i, _) = eof(i)?;

    Ok((
        i,
        PathMetadata {
            activity,
            product,
            institute,
            model,
            experiment,
            frequency,
            modeling_realm,
            variable,
            ensemble,
            mip_table,
            temporal_subset,
            geographical_info,
            version: Some(version),
        },
    ))
}

fn parse_esgf_filename(
    i: &str,
) -> IResult<&str, (Option<TemporalSubset>, Option<GeographicalInfo>)> {
    /*
    <variable_name>_<mip_table>_<model>_<experiment>_
    <ensemble_member>[_<temporal_subset>][_<geographical_info>]
    */

    let (i, _variable) = name_segment(i)?;
    let (i, _mip_table) = name_segment(i)?;
    let (i, _model) = name_segment(i)?;
    let (i, _experiment) = name_segment(i)?;
    // does not use `name_segment` because this could be the last part of the file and may not be followed by `_`
    let (i, _ensemble) = word(i)?;
    let (i, temporal_subset) = opt(parse_temporal_subset)(i)?;
    let (i, geographical_info) = opt(parse_geographical_info)(i)?;

    Ok((i, (temporal_subset, geographical_info)))
}

fn parse_esgf_gridspec_filename(
    i: &str,
) -> IResult<&str, (Option<TemporalSubset>, Option<GeographicalInfo>)> {
    // gridspec_<modeling_realm>_fx_<model>_<experiment>_r0i0p0
    let (i, _) = terminated(tag("gridspec"), name_sep)(i)?;
    let (i, _modeling_realm) = terminated(parse_modeling_realm, name_sep)(i)?;
    let (i, _) = terminated(tag("fx"), name_sep)(i)?;
    let (i, _model) = name_segment(i)?;
    let (i, _experiment) = name_segment(i)?;
    let (i, _ensemble) = parse_ensemble(i)?;
    Ok((i, (None, None)))
}

fn parse_frequency(i: &str) -> IResult<&str, Frequency> {
    map_res(word, Frequency::from_str)(i)
}

fn parse_modeling_realm(i: &str) -> IResult<&str, ModelingRealm> {
    map_res(word, ModelingRealm::from_str)(i)
}

pub(crate) fn parse_ensemble(i: &str) -> IResult<&str, EnsembleMember> {
    let (i, (realization, method, perturbed_num)) = tuple((
        preceded(tag("r"), nom_u32),
        preceded(tag("i"), nom_u32),
        preceded(tag("p"), nom_u32),
    ))(i)?;
    Ok((
        i,
        EnsembleMember {
            realization,
            initialization: method,
            physics: perturbed_num,
        },
    ))
}

fn parse_geographical_info(i: &str) -> IResult<&str, GeographicalInfo> {
    let (i, _) = tag("g-")(i)?;
    let (i, region) = parse_geographical_region(i)?;
    let (i, averaging) = opt(preceded(tag("-"), word))(i)?; // averaging has a - so we use `word` here

    Ok((i, GeographicalInfo { region, averaging }))
}

fn parse_geographical_region(i: &str) -> IResult<&str, Region> {
    alt((
        parse_geographical_region_bounding_box,
        parse_geographical_region_named,
    ))(i)
}

fn parse_geographical_region_bounding_box(i: &str) -> IResult<&str, Region> {
    let (i, (lat, lon)) = verify(
        pair(opt(parse_bounding_box_lat), opt(parse_bounding_box_lon)),
        |(lat, lon)| lat.is_some() || lon.is_some(),
    )(i)?;
    Ok((i, Region::BoundingBox { lat, lon }))
}

fn parse_geographical_region_named(i: &str) -> IResult<&str, Region> {
    let (i, name) = alpha1(i)?;
    Ok((i, Region::Named(name)))
}

fn parse_bounding_box_lat(i: &str) -> IResult<&str, &str> {
    let (i, _) = tag("lat")(i)?;
    recognize(tuple((digit1, tag("S"), digit1, tag("N"))))(i)
}

fn parse_bounding_box_lon(i: &str) -> IResult<&str, &str> {
    let (i, _) = tag("lon")(i)?;
    recognize(tuple((digit1, tag("W"), digit1, tag("E"))))(i)
}

#[cfg(test)]
mod tests {
    use crate::cmip5::{PartialDateTime, TemporalSubset};

    use super::*;

    #[test]
    fn test_frequency_parser() {
        assert!(parse_frequency("foo").is_err());
        use Frequency::*;
        let cases = vec![
            ("yr", Year),
            ("mon", Month),
            ("day", Day),
            ("6hr", Hour6),
            ("3hr", Hour3),
            ("subhr", SubHour),
            ("monClim", ClimatologicalMonthlyMean),
            ("fx", Fixed),
        ];
        for (s, expected) in cases.iter() {
            let res = parse_frequency(s);
            assert_eq!(
                res,
                Ok(("", *expected)),
                "input '{s}' did not give expected result '{expected:?}'"
            );
        }
    }

    #[test]
    fn test_parse_ensemble_member() {
        assert_eq!(
            Ok((
                "",
                EnsembleMember {
                    realization: 1,
                    initialization: 2,
                    physics: 3
                }
            )),
            parse_ensemble("r1i2p3")
        );
    }

    #[test]
    fn test_parse_region() {
        let cases = &[
            ("lat20S20Nlon20W20E", Some("20S20N"), Some("20W20E")),
            ("lon10W30E", None, Some("10W30E")),
        ];
        for (s, exp_lat, exp_lon) in cases.into_iter() {
            let expected = Region::BoundingBox {
                lat: exp_lat.clone(),
                lon: exp_lon.clone(),
            };
            assert_eq!(
                Ok(("", expected.clone())),
                parse_geographical_region(s),
                "input '{s} did not give expected result '{expected:?}'",
            );
        }
    }

    #[test]
    fn test_parse_geographical_info() {
        let cases = &[
            (
                "g-lon10W30E",
                Region::BoundingBox {
                    lat: None,
                    lon: Some("10W30E"),
                },
                None,
            ),
            (
                "g-lat10S20N",
                Region::BoundingBox {
                    lat: Some("10S20N"),
                    lon: None,
                },
                None,
            ),
            (
                "g-lat10S190Nlon50W40E-avg",
                Region::BoundingBox {
                    lat: Some("10S190N"),
                    lon: Some("50W40E"),
                },
                Some("avg"),
            ),
            ("g-namedregion", Region::Named("namedregion"), None),
            (
                "g-namedregion-some-avgfunc",
                Region::Named("namedregion"),
                Some("some-avgfunc"),
            ),
        ];
        for (s, region, suffix) in cases.into_iter() {
            let expected = GeographicalInfo {
                region: region.clone(),
                averaging: *suffix,
            };
            assert_eq!(
                Ok(("", expected.clone())),
                parse_geographical_info(s),
                "input '{s} did not give expected result '{expected:?}'",
            );
        }
    }

    #[test]
    fn test_full_esgf_paths() {
        let cases = full_esgf_path_data();
        for (i, (input, expected)) in cases.into_iter().enumerate() {
            let (remaining, output) = parse_esgf(input).expect("error parsing esgf path");
            assert_eq!(
                remaining, "",
                "expected no remaining data to parse on case {}",
                i
            );
            assert_eq!(
                expected, output,
                "output did not match expected on case {}",
                i
            );
        }
    }

    #[test]
    fn test_full_cmor_paths() {
        let cases = full_cmor_path_data();
        for (i, (input, expected)) in cases.into_iter().enumerate() {
            let (remaining, output) = parse_cmor(input).expect("error parsing cmor path");
            assert_eq!(
                remaining, "",
                "expected no remaining data to parse on case {}",
                i
            );
            assert_eq!(
                expected, output,
                "output did not match expected on case {}",
                i
            );
        }
    }

    fn full_esgf_path_data() -> Vec<(&'static str, PathMetadata<'static>)> {
        vec![
            (
                concat!(
                    "cmip5/output2/INM/inmcm4/esmHistorical/mon/land/Lmon/r1i1p1/v20110323/fVegSoil/",
                    "fVegSoil_Lmon_inmcm4_esmHistorical_r1i1p1_185001-200512.nc"
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "INM",
                    model: "inmcm4",
                    experiment: "esmHistorical",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Land,
                    variable: "fVegSoil",
                    ensemble: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    mip_table: "Lmon",
                    version: Some("v20110323"),
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(2005, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                },
            ),
            (
                concat!(
                    "cmip5/output2/INM/inmcm4/historical/mon/land/Lmon/r1i1p1/v20110323/mrlso/",
                    "mrlso_Lmon_inmcm4_historical_r1i1p1_185001-200512.nc"
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "INM",
                    model: "inmcm4",
                    experiment: "historical",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Land,
                    ensemble: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20110323"),
                    variable: "mrlso",
                    mip_table: "Lmon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(2005, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                },
            ),
            (
                concat!(
                    "cmip5/output2/INM/inmcm4/esmrcp85/mon/land/Lmon/r1i1p1/v20110323/residualFrac/",
                    "residualFrac_Lmon_inmcm4_esmrcp85_r1i1p1_200601-210012.nc",
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "INM",
                    model: "inmcm4",
                    experiment: "esmrcp85",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Land,
                    ensemble: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20110323"),
                    variable: "residualFrac",
                    mip_table: "Lmon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(2006, 1),
                        end: PartialDateTime::from_ym(2100, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                }
            ),
            (
                concat!(
                    "cmip5/output2/INM/inmcm4/piControl/mon/land/Lmon/r1i1p1/v20110323/fLuc/",
                    "fLuc_Lmon_inmcm4_piControl_r1i1p1_185001-234912.nc"
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "INM",
                    model: "inmcm4",
                    experiment: "piControl",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Land,
                    ensemble: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20110323"),
                    variable: "fLuc",
                    mip_table: "Lmon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(2349, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                }
            ),
            (
                concat!(
                    "cmip5/output2/NASA-GISS/GISS-E2-H/historical/mon/land/Lmon/r5i1p1/v20120517/cSoil/",
                    "cSoil_Lmon_GISS-E2-H_historical_r5i1p1_185001-190012.nc",
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "NASA-GISS",
                    model: "GISS-E2-H",
                    experiment: "historical",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Land,
                    ensemble: EnsembleMember {
                        realization: 5,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20120517"),
                    variable: "cSoil",
                    mip_table: "Lmon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(1900, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                }
            ),
            (
                concat!(
                    "cmip5/output2/IPSL/IPSL-CM5A-LR/amip4K/mon/atmos/aero/r1i1p1/v20110429/ps/",
                    "ps_aero_IPSL-CM5A-LR_amip4K_r1i1p1_197901-200912.nc"
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "IPSL",
                    model: "IPSL-CM5A-LR",
                    experiment: "amip4K",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Atmosphere,
                    ensemble: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20110429"),
                    variable: "ps",
                    mip_table: "aero",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1979, 1),
                        end: PartialDateTime::from_ym(2009, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                }
            ),
            (
                concat!(
                    "cmip5/output2/IPSL/IPSL-CM5A-LR/amip4K/mon/atmos/cfMon/r1i1p1/v20110429/ps/",
                    "ps_cfMon_IPSL-CM5A-LR_amip4K_r1i1p1_197901-200912.nc"
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "IPSL",
                    model: "IPSL-CM5A-LR",
                    experiment: "amip4K",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Atmosphere,
                    ensemble: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20110429"),
                    variable: "ps",
                    mip_table: "cfMon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1979, 1),
                        end: PartialDateTime::from_ym(2009, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                }
            ),
            (
                concat!(
                    "cmip5/output2/IPSL/IPSL-CM5A-LR/historical/mon/atmos/aero/r4i1p1/v20110406/ps/",
                    "ps_aero_IPSL-CM5A-LR_historical_r4i1p1_185001-200512.nc"
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "IPSL",
                    model: "IPSL-CM5A-LR",
                    experiment: "historical",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Atmosphere,
                    ensemble: EnsembleMember {
                        realization: 4,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20110406"),
                    variable: "ps",
                    mip_table: "aero",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(2005, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                }
            ),
            (
                concat!(
                    "cmip5/output2/IPSL/IPSL-CM5A-LR/historical/mon/atmos/cfMon/r2i1p1/v20111119/ps/",
                    "ps_cfMon_IPSL-CM5A-LR_historical_r2i1p1_185001-200512.nc"
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output2",
                    institute: "IPSL",
                    model: "IPSL-CM5A-LR",
                    experiment: "historical",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Atmosphere,
                    ensemble: EnsembleMember {
                        realization: 2,
                        initialization: 1,
                        physics: 1,
                    },
                    version: Some("v20111119"),
                    variable: "ps",
                    mip_table: "cfMon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(2005, 12),
                        suffix: None,
                    }),
                    geographical_info: None,
                }
            ),
            (
                concat!(
                    "cmip5/output1/NOAA-GFDL/GFDL-CM3/1pctCO2/fx/ocean/fx/r0i0p0/v20120227/gridspec/",
                    "gridspec_ocean_fx_GFDL-CM3_1pctCO2_r0i0p0.nc",
                ),
                PathMetadata {
                    activity: "cmip5",
                    product: "output1",
                    institute: "NOAA-GFDL",
                    model: "GFDL-CM3",
                    experiment: "1pctCO2",
                    frequency: Frequency::Fixed,
                    modeling_realm: ModelingRealm::Ocean,
                    ensemble: EnsembleMember {
                        realization: 0,
                        initialization: 0,
                        physics: 0,
                    },
                    version: Some("v20120227"),
                    variable: "gridspec",
                    mip_table: "fx",
                    temporal_subset: None,
                    geographical_info: None,
                }
            ),
        ]
    }

    fn full_cmor_path_data() -> Vec<(&'static str, PathMetadata<'static>)> {
        vec![
            (
                concat!(
                    "user-b/historical/MPI-M/MPI-ESM-LR/historical/mon/atmos/tas/r1i1p1/",
                    "tas_Amon_MPI-ESM-LR_historical_r1i1p1_185001-200512.nc"
                ),
                PathMetadata {
                    activity: "user-b",
                    product: "historical",
                    institute: "MPI-M",
                    model: "MPI-ESM-LR",
                    experiment: "historical",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Atmosphere,
                    variable: "tas",
                    ensemble: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    mip_table: "Amon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1850, 1),
                        end: PartialDateTime::from_ym(2005, 12),
                        suffix: None,
                    }),
                    version: None,
                    geographical_info: None,
                },
            ),
            (
                concat!(
                    "user-b/ancestry/FUB/MPI-ESM-LR/decs4e1988/mon/atmos/sic/r9i1p1/",
                    "sic_mon_MPI-ESM-LR_decs4e1988_r9i1p1_1989010100-1993123118.nc"
                ),
                PathMetadata {
                    activity: "user-b",
                    product: "ancestry",
                    institute: "FUB",
                    model: "MPI-ESM-LR",
                    experiment: "decs4e1988",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Atmosphere,
                    variable: "sic",
                    ensemble: EnsembleMember {
                        realization: 9,
                        initialization: 1,
                        physics: 1,
                    },
                    mip_table: "mon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ymd_h(1989, 1, 1, 0),
                        end: PartialDateTime::from_ymd_h(1993, 12, 31, 18),
                        suffix: None,
                    }),
                    version: None,
                    geographical_info: None,
                },
            ),
            (
                concat!(
                    "user-b/novolc/MPI-M/MPI-ESM-LR/decadal1970/mon/atmos/tas/r10i1p1/",
                    "tas_Amon_MPI-ESM-LR_decadal1970_r10i1p1_197101-198012.nc",
                ),
                PathMetadata {
                    activity: "user-b",
                    product: "novolc",
                    institute: "MPI-M",
                    model: "MPI-ESM-LR",
                    experiment: "decadal1970",
                    frequency: Frequency::Month,
                    modeling_realm: ModelingRealm::Atmosphere,
                    variable: "tas",
                    ensemble: EnsembleMember {
                        realization: 10,
                        initialization: 1,
                        physics: 1,
                    },
                    mip_table: "Amon",
                    temporal_subset: Some(TemporalSubset {
                        start: PartialDateTime::from_ym(1971, 1),
                        end: PartialDateTime::from_ym(1980, 12),
                        suffix: None,
                    }),
                    version: None,
                    geographical_info: None,
                },
            ),
        ]
    }
}
