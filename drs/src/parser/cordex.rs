use std::str::FromStr;

use nom::{
    bytes::complete::tag,
    combinator::{eof, map_res, opt},
    sequence::{preceded, terminated},
    IResult,
};

use crate::{
    cmip5::PartialDateTime,
    cordex::{Frequency, PathMetadata},
    parser::{
        cmip5::parse_ensemble,
        common::{name_segment, name_sep, parse_time, parse_version, path_sep},
    },
};

use super::common::{path_segment, word};

/// Parses a string as a CORDEX path. This assumes that the string it's given will be just the portion of a path that
/// is relevant to CORDEX meaning any preceding directories have already been removed.
pub(crate) fn parse_cordex(path: &str) -> IResult<&str, PathMetadata> {
    /*
    <activity>/<product>/<domain>/<institution>/
    <gcm_model_name>/<cmip5_experiement_name>/<cmip5_ensemble_member>/
    <rcm_model_name>/<rcm_version_id>/<frequency>/<variable_name>/[<version>/]
    */
    let (i, activity) = path_segment(path)?;
    let (i, product) = path_segment(i)?;
    let (i, domain) = path_segment(i)?;
    let (i, institution) = path_segment(i)?;
    let (i, gcm_model_name) = path_segment(i)?;
    let (i, cmip5_experiment_name) = path_segment(i)?;
    let (i, cmip5_ensemble_member) = terminated(parse_ensemble, path_sep)(i)?;
    let (i, rcm_model_name) = path_segment(i)?;
    let (i, rcm_version_id) = path_segment(i)?;
    let (i, frequency) = terminated(parse_frequency, path_sep)(i)?;
    let (i, variable_name) = path_segment(i)?;
    let (i, version) = opt(terminated(parse_version, path_sep))(i)?;

    /*
    <variable_name>_<domain>_<gcm_model_name>_
    <cmip5_experiment_name>_<cmip5_ensemble_member>_
    <rcm_model_name>_<rcm_version_id>_<frequency>[_<start_time>-<end_time>].nc
    */
    let (i, _variable) = name_segment(i)?;
    let (i, _domain) = name_segment(i)?;
    let (i, _gcm_model_name) = name_segment(i)?;
    let (i, _cmip5_experiment_name) = name_segment(i)?;
    let (i, _cmip5_ensemble_member) = terminated(parse_ensemble, name_sep)(i)?;
    let (i, _rcm_model_name) = name_segment(i)?;
    let (i, _rcm_version_id) = name_segment(i)?;
    // no `terminated path_sep` due to this being the last unless followed by times
    let (i, _frequency) = parse_frequency(i)?;
    let (i, times) = opt(preceded(name_sep, parse_start_end_time))(i)?;

    let (i, _) = tag(".nc")(i)?;
    let (i, _) = eof(i)?;

    Ok((
        i,
        PathMetadata {
            activity,
            product,
            domain,
            institution,
            gcm_model_name,
            cmip5_experiment_name,
            cmip5_ensemble_member,
            rcm_model_name,
            rcm_version_id,
            frequency,
            variable_name,
            version,
            time_range: times,
        },
    ))
}

fn parse_frequency(i: &str) -> IResult<&str, Frequency> {
    map_res(word, Frequency::from_str)(i)
}

fn parse_start_end_time(i: &str) -> IResult<&str, (PartialDateTime, PartialDateTime)> {
    let (i, start) = parse_time(i)?;
    let (i, _) = tag("-")(i)?;
    let (i, end) = parse_time(i)?;

    Ok((i, (start, end)))
}

#[cfg(test)]
mod tests {
    use crate::cmip5::EnsembleMember;

    use super::*;

    #[test]
    fn test_full_paths() {
        let cases = full_path_data();
        for (i, (input, expected)) in cases.into_iter().enumerate() {
            let (remaining, output) = parse_cordex(input).expect("error parsing esgf path");
            assert_eq!(
                remaining, "",
                "expected no remaining data to parse on case {}",
                i
            );
            assert_eq!(
                expected, output,
                "output did not match expected on case {}",
                i
            );
        }
    }

    fn full_path_data() -> Vec<(&'static str, PathMetadata<'static>)> {
        vec![
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/rsdt/v20131211/",
                    "rsdt_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_19910101-19951231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "rsdt",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(1991,1,1), PartialDateTime::from_ymd(1995,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/prw/v20131211/",
                    "prw_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_19960101-20001231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "prw",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(1996,1,1), PartialDateTime::from_ymd(2000,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/ta500/v20131211/",
                    "ta500_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_20060101-20101231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "ta500",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(2006,1,1), PartialDateTime::from_ymd(2010,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/tauu/v20131211/",
                    "tauu_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_19910101-19951231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "tauu",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(1991,1,1), PartialDateTime::from_ymd(1995,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/rlut/v20131211/",
                    "rlut_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_20010101-20051231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "rlut",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(2001,1,1), PartialDateTime::from_ymd(2005,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/ua500/v20131211/",
                    "ua500_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_19960101-20001231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "ua500",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(1996,1,1), PartialDateTime::from_ymd(2000,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/tauv/v20131211/",
                    "tauv_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_20110101-20111130.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "tauv",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(2011,1,1), PartialDateTime::from_ymd(2011,11,30))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/hus850/v20131211/",
                    "hus850_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_20110101-20111130.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "hus850",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(2011,1,1), PartialDateTime::from_ymd(2011,11,30))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/evspsbl/v20131211/",
                    "evspsbl_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_20010101-20051231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "evspsbl",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(2001,1,1), PartialDateTime::from_ymd(2005,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/va200/v20131211/",
                    "va200_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_19900101-19901231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "va200",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(1990,1,1), PartialDateTime::from_ymd(1990,12,31))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/sfcWindmax/v20131211/",
                    "sfcWindmax_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_20110101-20111130.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "sfcWindmax",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(2011,1,1), PartialDateTime::from_ymd(2011,11,30))),
                }
            ),
            (
                concat!(
                    "cordex/output/AFR-44/MOHC/ECMWF-ERAINT/evaluation/r1i1p1/MOHC-HadRM3P/v1/day/zmla/v20131211/",
                    "zmla_AFR-44_ECMWF-ERAINT_evaluation_r1i1p1_MOHC-HadRM3P_v1_day_20010101-20051231.nc"
                ),
                PathMetadata {
                    activity: "cordex",
                    product: "output",
                    domain: "AFR-44",
                    institution: "MOHC",
                    gcm_model_name: "ECMWF-ERAINT",
                    cmip5_experiment_name:"evaluation",
                    cmip5_ensemble_member: EnsembleMember {
                        realization: 1,
                        initialization: 1,
                        physics: 1,
                    },
                    rcm_model_name: "MOHC-HadRM3P",
                    rcm_version_id: "v1",
                    frequency: Frequency::Day,
                    variable_name: "zmla",
                    version: Some("v20131211"),
                    time_range: Some((PartialDateTime::from_ymd(2001,1,1), PartialDateTime::from_ymd(2005,12,31))),
                }
            )
        ]
    }
}
