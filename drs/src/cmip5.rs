//! <https://pcmdi.llnl.gov/mips/cmip5/docs/cmip5_data_reference_syntax.pdf>
//! See section 3.1
//!
//! CMIP5 defines two different directory structures, an older CMOR structure and a newer structure which is what
//! should be used going forward. This module will map both into the same [`Cmip5`] representation.
//!
//! Old CMOR directory structure:
//! ```text
//! <activity>/<product>/<institute>/<model>/<experiment>/
//! <frequency>/<modeling_realm>/<variable_name>/<ensemble_member>
//! ```
//! New CMIP5/ESGF directory structure:
//! ```text
//! <activity>/<product>/<institute>/<model>/<experiment>/<frequency>/
//! <modeling_realm>/<mip_table>/<ensemble_member>/<version>/
//! <variable_name>
//! ```
//! CMIP5 file name structure:
//! ```text
//! <variable_name>_<mip_table>_<model>_<experiment>_
//! <ensemble_member>[_<temporal_subset>][_<geographical_info>]
//! ```
//! CMIP5 file names have an alternative form for gridspec files:
//! ```text
//! gridspec_<modeling_realm>_fx_<model>_<experiment>_r0i0p0.nc
//! ```
//! Currently those differences will be ignored because a file being a gridspec file or not does not seem to be relevant
//! to the rest of its metadata.
//!
//! Publication level dataset (see 3.4)
//! Datasets are assigned an ID with THREDDS catalogs ESGF nodes of the form:
//! ```text
//! <activity>.<product>.<institute>.<model>.<experiment>.<frequency>.<modeling_realm>.
//! <mip_tables>.<ensemble_member>
//! ```
//! Each publication-level dataset _version_ will have the ID:
//! ```text
//! <activity>.<product>.<institute>.<model>.<experiment>.<frequency>.<modeling_realm>.
//! <mip_tables>.<ensemble_member>.<version>
//! ```
//! The only difference between this and the dataset ID is the presence of `<version>`.
//!
//! Exceptions to the CMIP5 standard:
//! * According to the spec, `activity` and `product` must be `cmip5` and one of `output`, `output1`, `output2`, or
//!     `unsolicited` respectively. This will instead allow any values for `activity` and `product`.
//! * This allows frequency values of `1hr` in addition to the normal values

use std::str::FromStr;

use camino::Utf8Path;
use thiserror::Error;
use tracing::error;

use crate::parser::common::parse_time;

use super::parser::{parse_cmor, parse_esgf};

/// Error extracting data from a cmor path
#[derive(Debug, Error)]
#[error("error parsing CMOR path: {0}")]
pub struct InvalidCmorPathError(String);

/// Error extracting data from an esgf path
#[derive(Debug, Error)]
#[error("error parsing ESGF path: {0}")]
pub struct InvalidEsgfPathError(String);

/// Holds path and metadata pulled from a CMIP5 style path.
#[derive(Debug)]
pub struct Cmip5<'a> {
    /// The path of the file relative to the root directory of the dataset meaning it contains all of and only the
    /// elements relevant to CMIP5.
    pub path: &'a Utf8Path,
    /// The metadata extracted from the path
    pub metadata: PathMetadata<'a>,
}

impl<'a> Cmip5<'a> {
    /// Extracts metadata from a CMOR style file path. It expects that the path will consist of only the parts relevant
    /// to the DRS structure. For example, if a dataset starts at `/foo/data/cmip5/output...` then `path` must start
    /// from `cmip5/output...`.
    ///
    /// Note:
    /// * This will not verify that the file upholds various cross-field constraints of the spec  (e.g. that `fx`
    ///     frequency has an ensemble value of `r0i0p0`).
    pub fn from_cmor_path(path: &Utf8Path) -> Result<Cmip5, InvalidCmorPathError> {
        let metadata = match parse_cmor(path.as_str()) {
            Ok((_, metadata)) => metadata,
            Err(e) => return Err(InvalidCmorPathError(e.to_string())),
        };

        Ok(Cmip5 { path, metadata })
    }

    /// Extracts metadata from an ESGF style file path. It expects that the path will consist of only the parts relevant
    /// to the DRS structure. For example, if a dataset starts at `/foo/data/cmip5/output...` then `path` must start
    /// from `cmip5/output...`.
    ///
    /// Note:
    /// * This will not verify that the file upholds various cross-field constraints of the spec  (e.g. that `fx`
    ///     frequency has an ensemble value of `r0i0p0`).
    pub fn from_esgf_path(path: &Utf8Path) -> Result<Cmip5, InvalidEsgfPathError> {
        let metadata = match parse_esgf(path.as_str()) {
            Ok((_, metadata)) => metadata,
            Err(e) => return Err(InvalidEsgfPathError(e.to_string())),
        };

        Ok(Cmip5 { path, metadata })
    }
}

/// CMIP5 metadata encoded in the file path and file name of a file organized according to the CMIP5 DRS specification.
///
/// Field documentation is partially copied from source document where relevant for a basic overview of the meaning of
/// the field and information on extracting and verifying its value.
#[derive(Debug, PartialEq, Eq)]
pub struct PathMetadata<'a> {
    /// Identifies the model intercomparison activity or other data collection activity.
    ///
    /// There does not seem to be a definitive list of all possible values for this so no value checking will be done.
    pub activity: &'a str,
    /// Roughly, a description of what has been done to reach this data. From the spec:
    ///
    /// > For CMIP5, files will initially be designated as "output" or "unsolicited". Subsequently, data from the
    /// > requested variable list will be assigned a version (see below) and placed in either "output1" or "output2."
    ///
    /// The spec says the allowed names are `output`, `output1`, `output2`, and `unsolicited` but later states that
    /// other values may appear here but this is not part of the current DRS. Since current usage examples already use
    /// other values, this will not check if the value is one of the stated possible values and will accept any string.
    pub product: &'a str,
    /// Identifies the institute responsible for the model results. or CMIP5 the institute name will be suggested by the
    /// research group at the institute, subject to final authorization by PCMDI May differ from the netcdf
    /// `institute_id` value.
    ///
    /// There does not seem to be a definitive list of all possible values so no checking will be done.
    pub institute: &'a str,
    /// Identifies the model used. Subject to certain constraints imposed by PCMDI, the modeling group will assign this
    /// name, which might include a version number (usually truncated to the nearest integer). May differ from the netcdf
    /// attribute `model_id` value.
    ///
    /// There does not seem to be a definitive list of all possible values so no checking will be done.
    pub model: &'a str,
    /// Identifies either the experiment or both the experiment _family_ and the specific _type_ within that experiment
    /// family. These experiment names are not freely chosen, but come from controlled vocabularies defined in the
    /// Appendix 1.1 of the source document under the column labeled “Short Name of Experiment”
    pub experiment: &'a str,
    /// The interval between time samples in this dataset. See [`Frequency`] for more details.
    pub frequency: Frequency,
    /// Modeling component most relevant to this dataset. See [`ModelingRealm`] for more details.
    pub modeling_realm: ModelingRealm,
    /// This and the MIP table component identify the physical quantity and often imply something about the sampling
    /// frequency and modeling realm. For CMIP5 the variable anme and MIP table for requested output appear in the
    /// `standard_output` spreadsheet in <https://pcmdi.llnl.gov/mips/cmip5/requirements.html>.
    ///
    /// Note that hyphens (-) are forbidden in CMIP5 variable names. Though later the document states that this is
    /// merely recommended for programming language compatibility. This chooses to allow hyphens.
    pub variable: &'a str,
    /// Informations about what variable conditions were applied to this dataset. See [`EnsembleMember`] for more
    /// details.
    pub ensemble: EnsembleMember,
    /// For CMIP5, each MIP table contains fields sample only at a single frequency. See also `variable_name`.
    pub mip_table: &'a str,
    /// The version number will be `v` followed by an integer, which uniquely identifies a particular version of a
    /// publication-level dataset. For CMIP5 the version number is supposed to reflect the date of publication: for
    /// example, `v20100105` for a version provided on the 5th January 2010. Software interpreting version numbers
    /// should not, however, assume the integer has invariably been correctly encoded (e.g., sometimes a single digit
    /// number might appear as in `v3`).
    pub version: Option<&'a str>,
    /// The time interval covered by this dataset. See [`TemporalSubset`] for more details.
    pub temporal_subset: Option<TemporalSubset<'a>>,
    /// The geographical region covered by this dataset. See [`GeographicalInfo`] for more details.
    pub geographical_info: Option<GeographicalInfo<'a>>,
}

/// Indicates the interval between individual time-samples in the atomic dataset. For CMIP5, the following are the
/// only options: `yr`, `mon`, `day`, `6hr`, `3hr`, `subhr`, `monClim`, and `fx`.
///
/// This additionally allows values of `1hr` corresponding to 1 hourly data.
///
/// Further information: <https://pcmdi.llnl.gov/mips/cmip5/requirements.html>
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Frequency {
    /// Yearly frequency
    Year,
    /// Monthly frequency
    Month,
    /// Daily frequency
    Day,
    /// 6 hourly frequency
    Hour6,
    /// 3 hourly frequency
    Hour3,
    /// Hourly frequency.
    ///
    /// This value is not part of the CMIP5 DRS spec but is frequently used
    Hour1,
    // TODO: it would be nice to have more details on these 2
    /// Sub-hourly frequency
    SubHour,
    /// Climatological monthly mean frequency
    ClimatologicalMonthlyMean,
    /// This dataset does not have data that changes over time
    Fixed,
}

impl ToString for Frequency {
    fn to_string(&self) -> String {
        use Frequency::*;
        let s = match self {
            Year => "yr",
            Month => "mon",
            Day => "day",
            Hour6 => "6hr",
            Hour3 => "3hr",
            Hour1 => "1hr",
            SubHour => "subhr",
            ClimatologicalMonthlyMean => "monclim",
            Fixed => "fx",
        };
        s.to_owned()
    }
}

/// Error parsing a string value into a frequency
#[derive(Debug, Error)]
#[error("invalid frequency value {given}")]
pub struct InvalidFrequencyError {
    given: String,
}

impl FromStr for Frequency {
    type Err = InvalidFrequencyError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Frequency::*;
        let s = s.to_lowercase();
        match s.as_str() {
            "yr" => Ok(Year),
            "mon" => Ok(Month),
            "day" => Ok(Day),
            "6hr" => Ok(Hour6),
            "3hr" => Ok(Hour3),
            "1hr" => Ok(Hour1),
            "subhr" => Ok(SubHour),
            "monclim" => Ok(ClimatologicalMonthlyMean),
            "fx" => Ok(Fixed),
            _ => Err(InvalidFrequencyError { given: s }),
        }
    }
}

/// Indicates which high level modeling component is of particular relevance for the dataset. For CMIP5, the
/// permitted values are: `atmos`, `ocean`, `land`, `landIce`, `seaIce`, `aerosol`, `atmosChem`, and `ocnBgchem`.
/// Note that sometimes a variable will be equally (or almost equally relevant) to two or more “realms”, in which
/// case the atomic dataset might be assigned to a primary “realm”, but cross-referenced or aliased to the other
/// relevant “realms”
///
/// Its `FromStr` is case insensitive so `landice`, `landIce`, etc. would all be considered `LandIce`.
///
/// Further information: <https://pcmdi.llnl.gov/mips/cmip5/requirements.html>
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ModelingRealm {
    /// Dataset applies to atmospheric realm
    Atmosphere,
    /// Dataset applies to ocean
    Ocean,
    /// Dataset applies to land
    Land,
    /// Dataset applies to land ice
    LandIce,
    /// Dataset applies to sea ice
    SeaIce,
    /// Dataset applies to aerosol
    Aerosol,
    /// Dataset applies to atmospheric chemicals
    AtmosphereChemical,
    /// Dataset applies to ocean biogeochemical
    OceanBiogeochemical,
}

impl ToString for ModelingRealm {
    fn to_string(&self) -> String {
        use ModelingRealm::*;
        let s = match self {
            Atmosphere => "atmos",
            Ocean => "ocean",
            Land => "land",
            LandIce => "landice",
            SeaIce => "seaice",
            Aerosol => "aerosol",
            AtmosphereChemical => "atmoschem",
            OceanBiogeochemical => "ocnbgchem",
        };
        s.to_owned()
    }
}

/// Error parsing modeling realm from a string
#[derive(Debug, Error)]
#[error("invalid modeling realm value {given}")]
pub struct InvalidModelingRealmError {
    given: String,
}

impl FromStr for ModelingRealm {
    type Err = InvalidModelingRealmError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ModelingRealm::*;
        let s = s.to_lowercase();
        match s.as_str() {
            "atmos" => Ok(Atmosphere),
            "ocean" => Ok(Ocean),
            "land" => Ok(Land),
            "landice" => Ok(LandIce),
            "seaice" => Ok(SeaIce),
            "aerosol" => Ok(Aerosol),
            "atmoschem" => Ok(AtmosphereChemical),
            "ocnbgchem" => Ok(OceanBiogeochemical),
            _ => Err(InvalidModelingRealmError { given: s }),
        }
    }
}

/// A tuple of 3 integers formatted as `r<N>i<M>p<L>` distinguishes among closely related simulations by a single
/// model. All three are required even if only a single simulation is performed. In CMIP5, time-independent
/// variables (i.e., those with frequency=`fx`) are not expected to differ across ensemble members, so for these M
/// should invariably be assigned the value zero (`i0`). The same holds true for the other numbers.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct EnsembleMember {
    /// Used to distinguish between members of an ensemble that are generated by initializing a set of runs with
    /// different, equally realistic initial conditions.
    pub realization: u32,
    /// Models used for forecasts that depend on the initial conditions might be initialized from observations using
    /// different methods or different observational datasets. This value distinguishes between conditions
    pub initialization: u32,
    /// The "perturbed physics" numbers used to distinguish between closely-related model versions which are, as a
    /// group, referred to as a perturbed physics ensemble where the different models use different sets of model
    /// parameters.
    pub physics: u32,
}

impl ToString for EnsembleMember {
    fn to_string(&self) -> String {
        format!(
            "r{0}i{1}p{2}",
            self.realization, self.initialization, self.physics
        )
    }
}

/// Error parsing ensemble member from a string
#[derive(Debug, Error)]
#[error("invalid ensemble member string: {reason}")]
pub struct InvalidEnsembleMember {
    reason: String,
}

impl FromStr for EnsembleMember {
    type Err = InvalidEnsembleMember;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (_, e) = match super::parser::cmip5::parse_ensemble(s) {
            Ok(res) => res,
            Err(e) => {
                return Err(InvalidEnsembleMember {
                    reason: e.to_string(),
                })
            }
        };

        Ok(e)
    }
}

/// Time instants or periods will be represented by a construction of the form `N1-N2`, where N1 and N2 are of the
/// form `yyyy[MM[dd[hh[mm[ss]]]]][-suffix]`, where `yyyy`, `MM`, `dd`, `hh`, `mm`, and `ss` are integer year,
/// month, day, hour, minute, and second, respectively, and the precision with which time is expressed must
/// unambiguously resolve the interval between time-samples contained in the file or virtual file. If only a single
/// time instant is included in the dataset, N2 may normally be omitted, but for CMIP5 N2 is required and in this
/// case would be identical to N1.
///
/// Note that the DRS does not explicitly specify the calendar type (e.g., Julian, Gregorian), but the calendar will
/// be indicated by one of the attributes in each netCDF file. This is omitted for variables that are
/// time-independent.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TemporalSubset<'a> {
    /// Start of the time period
    pub start: PartialDateTime,
    /// End of the time period
    pub end: PartialDateTime,
    /// The optional `-suffix` can be included to indicate that the netCDF file contains a climatology (suffix =
    /// `-clim`) or a single time mean, for example, over multiple years (suffix = `-avg`). Consider a file containing a
    /// single time-average, based on daily samples for the two-week period from February 1, 1971 through February 14,
    /// 1971. In this case the frequency for the dataset would be `day` (because the average is based on daily samples),
    /// and the suffix would be `19710201-19710214-avg`
    pub suffix: Option<&'a str>,
}

/// The partially initialized times that can be set in DRS files. They are of the form `YYYY[MM[dd[hh[mm[ss]]]]]`
/// meaning that only year is required but each further value requires all the preceeding values to be present i.e.
/// there can't be a time consisting of only years and seconds
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PartialDateTime {
    /// year, consists of 4 characters
    pub year: i32,
    /// month, consists of 2 characters
    pub month: Option<u32>,
    /// day, consists of 2 characters
    pub day: Option<u32>,
    /// hour, consists of 2 characters (24 hour)
    pub hour: Option<u32>,
    /// minute, consists of 2 characters
    pub minute: Option<u32>,
    /// second, consists of 2 characters
    pub second: Option<u32>,
}

impl PartialDateTime {
    // I'm not sure this is the best way to handle construction of this thing but

    /// Creates a [`Self`] with only a `year` value.
    pub fn from_y(year: i32) -> Self {
        Self {
            year,
            month: None,
            day: None,
            hour: None,
            minute: None,
            second: None,
        }
    }

    /// Creates a [`Self`] with `year` and `month`.
    pub fn from_ym(year: i32, month: u32) -> Self {
        let mut s = Self::from_y(year);
        s.month = Some(month);
        s
    }

    /// Creates a [`Self`] with `year`, `month`, and `day`.
    pub fn from_ymd(year: i32, month: u32, day: u32) -> Self {
        let mut s = Self::from_ym(year, month);
        s.day = Some(day);
        s
    }

    /// Creates a [`Self`] with `year`, `month`, `day`, and `hour`.
    pub fn from_ymd_h(year: i32, month: u32, day: u32, hour: u32) -> Self {
        let mut s = Self::from_ymd(year, month, day);
        s.hour = Some(hour);
        s
    }

    /// Creates a [`Self`] with `year`, `month`, `day`, `hour`, and `minute`.
    pub fn from_ymd_hm(year: i32, month: u32, day: u32, hour: u32, minute: u32) -> Self {
        let mut s = Self::from_ymd_h(year, month, day, hour);
        s.minute = Some(minute);
        s
    }

    /// Creates a [`Self`] with `year`, `month`, `day`, `hour`, `minute`, and `second`.
    pub fn from_ymd_hms(
        year: i32,
        month: u32,
        day: u32,
        hour: u32,
        minute: u32,
        second: u32,
    ) -> Self {
        let mut s = Self::from_ymd_hm(year, month, day, hour, minute);
        s.second = Some(second);
        s
    }
}

/// Error when extracting a [`PartialDateTime`] from a string.
#[derive(Debug, Error)]
#[error("invalid partial date time given: {reason}")]
pub struct InvalidPartialDateTime {
    reason: String,
}

impl FromStr for PartialDateTime {
    type Err = InvalidPartialDateTime;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        parse_time(s)
            .map(|(_, p)| p)
            .map_err(|e| InvalidPartialDateTime {
                reason: e.to_string(),
            })
    }
}

/// The geographical indicator is always optional, but when present it should appear last in the extended path. This
/// indicator specifies geographical subsets described by bounding boxes (e.g.  20S to 20N and 0 to 180E) or by
/// named regions (e.g., 'pacific-ocean`). The underscore character (`_`) is forbidden in the geographical
/// indicator.
///
/// The DRS specification for this indicator is a string of the form `g-XXXX[-YYYY].` The `g-` indicates that some
/// spatial selection or processing has been done (i.e., selection of a sub-global region and possibly spatial
/// averaging).  The `XXXX`, which must not be omitted, is either a named region (with names from a specific
/// gazetteer, which is yet to be selected) or the bounds of a latitude-longitude rectangle (following the template
/// defined below). The `YYYY` is optional and indicates if and what sort of spatial averaging has been performed
/// and whether the average includes masking of certain areas within the region (e.g., masking of land areas). The
/// DRS currently includes a single named region: `global`, which is used to select data from the entire horizontal
/// domain.
///
/// In the case of a bounding box, the bounds of the region should be specified following the template,
/// `latJHJJHHlonMZMMZZ` where J, JJ, M and MM are integers, indicating the southern, northern, western and eastern
/// edges of the bounding box, respectively. H and HH are restricted to `N` or `S` (indicating "north" or "south"),
/// and the Z and ZZ are restricted to `E` or `W` (indicating "east" or "west"). The longitude values should be in
/// the range from 0 to 180 (e.g., a box spanning 200 degrees of longitude could be specified by `10W170W`, but not
/// by `10W190E`, even though 170W and 190E are the same longitude). The latitude and longitude values should be
/// rounded to the nearest integer. Omission of the latitude range or the longitude range implies that data were
/// selected from the entire domain of the omitted dimension. (For example, `lat20S20N` implies all longitudes were
/// included.) Remember, however, that if `XXXX` designates a bounding box, then at least one of the dimensions must
/// appear.
///
/// The `YYYY` string is of the form `[yyy]-[zzz]` where the hyphen should be omitted unless both `yyy` and `zzz`
/// are present. As options for `yyy`, the DRS currently includes `lnd` and `ocn`.  The `lnd` suffix indicates that
/// only `land` locations are considered, and the `ocn` suffix indicates that only `ocean` locations (including sea
/// ice) are considered. As options for `zzz`, the DRS currently includes `zonalavg` and `areaavg`, which indicate
/// `zonal mean` and `area mean` respectively.
///
/// Here are some examples of geographical indicators:
/// * `g-lat20S20Nlon170W130W”`– a geographical subset defined by a bounding box (latitudes -20 to 20, and
///     longitudes -170 to -130, when rounded to the nearest integer)
/// * `g-global-ocn-areaavg` – an average over the world’s oceans.
/// * `g-lat20S20N` – a geographical subset defined by a bounding box covering all longitudes
///     and extending from 20S to 20N.
/// * `g-lat20S20N-lnd-zonalavg` – a zonal average over tropical lands, covering all
///     longitudes.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GeographicalInfo<'a> {
    /// The region of the file
    pub region: Region<'a>,
    /// How the data has been averaged across the region
    pub averaging: Option<&'a str>,
}

/// The different types of region definitions
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Region<'a> {
    /// A region defined by a latitude-longitude bounding box
    BoundingBox {
        /// the latitude of the bounding box, both north and south bounds
        lat: Option<&'a str>,
        /// the longitude of the bounding box, both east and west bounds
        lon: Option<&'a str>,
    },
    /// A region defined by a single name, e.g. `global`.
    Named(&'a str),
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_frequency_from_str() {
        use Frequency::*;
        let cases = vec![
            ("yr", Year),
            ("mon", Month),
            ("day", Day),
            ("6hr", Hour6),
            ("3hr", Hour3),
            ("1hr", Hour1),
            ("subhr", SubHour),
            ("monClim", ClimatologicalMonthlyMean),
            ("fx", Fixed),
        ];
        for (s, expected) in cases.iter() {
            let res = Frequency::from_str(s);
            assert!(res.is_ok(), "unexpected error on from_str for '{s}'");
            assert_eq!(
                res.unwrap(),
                *expected,
                "input '{s}' did not give expected result '{expected:?}'"
            );
        }
    }

    #[test]
    fn test_modeling_realm_from_str() {
        use ModelingRealm::*;
        let cases = vec![
            ("atmos", Atmosphere),
            ("ocean", Ocean),
            ("land", Land),
            ("landIce", LandIce),
            ("seaIce", SeaIce),
            ("aerosol", Aerosol),
            ("atmosChem", AtmosphereChemical),
            ("ocnBgchem", OceanBiogeochemical),
        ];
        for (s, expected) in cases.iter() {
            let res = ModelingRealm::from_str(s);
            assert!(res.is_ok(), "unexpected error on from_str for '{s}'");
            assert_eq!(
                res.unwrap(),
                *expected,
                "input '{s}' did not give expected result '{expected:?}'"
            );
        }
    }
}
