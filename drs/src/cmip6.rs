//! Implementation of the [CMIP6
//! specification](https://docs.google.com/document/d/1h0r8RZr_f3-8egBMMh7aqLwy3snpD6_MrDz1q8n5XUk/edit) (see "File
//! name template:" and the preceding section) for extracting metadata from a file's path and name.
//!
//! Directory structure:
//! ```text
//! <mip_era>/<activity_id>/<institution_id>/<source_id>/<experiment_id>/
//! <member_id>/<table_id>/<variable_id>/<grid_label>/<version>/<filename>
//! ```
//!
//! Filename structure:
//! ```text
//! <variable_id>_<table_id>_<source_id>_<experiment_id>_<member_id>_
//! <grid_label>[_<time_range>].nc
//! ```
//!

use camino::Utf8Path;
use thiserror::Error;

use crate::{cmip5::TemporalSubset, parser::parse_cmip6};

/// The required value for the mip_era field of CMIP6 paths.
pub const MIP_ERA: &str = "CMIP6";

/// Error parsing cmip6 data from a path
#[derive(Debug, Error)]
#[error("invalid cmip6 path: {reason}")]
pub struct InvalidCmip6PathError {
    reason: String,
}

/// Holds data for a CMIP6 path
pub struct Cmip6<'a> {
    /// The path of the file relative to the root directory of the dataset meaning it contains all of and only the
    /// elements relevant to CMIP6.
    pub path: &'a Utf8Path,
    /// The metadata extracted from the path
    pub metadata: PathMetadata<'a>,
}

impl<'a> Cmip6<'a> {
    /// Extracts metadata from a CMIP6 style file path. It expects that the path will consist of only the parts relevant
    /// to the DRS structure. For example, if a dataset starts at `/foo/data/cmip6/output...` then `path` must start
    /// from `cmip6/output...`.
    ///
    /// Note:
    /// * This will not verify that the file upholds various cross-field constraints of the spec  (e.g. that `fx`
    ///     frequency has an ensemble value of `r0i0p0`).
    pub fn from_path(path: &'a Utf8Path) -> Result<Cmip6, InvalidCmip6PathError> {
        let metadata = match parse_cmip6(path.as_str()) {
            Ok((_, metadata)) => metadata,
            Err(e) => {
                return Err(InvalidCmip6PathError {
                    reason: e.to_string(),
                })
            }
        };

        Ok(Self { path, metadata })
    }
}

/// Metadata related to files stored according to the CMIP6 standard.
///
/// Fields when have controlled vocabularies that are not static will not have their values checked against the
/// CV list.
#[derive(Debug, PartialEq, Eq)]
pub struct PathMetadata<'a> {
    /// Activity ID, equivalent to CMIP5's `activity`.
    ///
    /// Allowed values and their meanings are
    /// [here](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_activity_id.json).
    pub activity_id: &'a str,
    /// Institution ID, equivalent of CMIP5's `institution`.
    ///
    /// Allowed values and their meanings are
    /// [here](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_institution_id.json)
    pub institution_id: &'a str,
    /// Model identifier, equivalent of CMIP5's `model`.
    ///
    /// Allowed values and their meanings are
    /// [here](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_source_id.json).
    pub source_id: &'a str,
    /// Root experiment ID, similar to CMIP5's `experiment`.
    ///
    /// Allowed values and their meanings
    /// [here](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_experiment_id.json)
    pub experiment_id: &'a str,
    /// Information on the experiment and variant label (similar to ensemble member in other DRS specs).
    pub member_id: MemberId<'a>,
    /// Table identifier, equivalent of CMIP5's `table_id`.
    ///
    /// Allowed values [here](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_table_id.json)
    pub table_id: &'a str,
    /// Name of the variable contained in this dataset.
    pub variable_id: &'a str,
    /// Grid label to identify if this data uses the model's native grid, a different grid (i.e. was regridded) or no
    /// grid in the case of some form of averaging.
    ///
    /// Allowed values [here](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_grid_label.json).
    pub grid_label: &'a str,
    /// Version for the data, according to the spec this will always have the form `vYYYYMMDD`.
    pub version: &'a str,
    /// The time range covered by this dataset
    pub time_range: Option<TemporalSubset<'a>>,
}

/// Compound field consisting of `[<sub_experiment_id>-]<variant_label>`
#[derive(Debug, PartialEq, Eq)]
pub struct MemberId<'a> {
    /// Sub-experiment ID. Needed for CMIP6 hindcast and forecast experiments to indicate "start year".  For other
    /// experiments, this should be set to "none".
    ///
    /// Allowed values and meanings
    /// [here](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_sub_experiment_id.json)
    pub sub_experiment_id: Option<&'a str>,
    /// The variant label for this data
    pub variant_label: VariantLabel,
}

/// Information about the variant used to generate this data, equivalent to CMIP5's `ensemble`.
#[derive(Debug, PartialEq, Eq)]
pub struct VariantLabel {
    /// Used to distinguish between members of an ensemble that differ in their initial conditions
    pub realization: u32,
    /// Used to distinguish simulations performed under the same conditions but with different initialization procedures
    pub initialization: u32,
    /// Used to distinguish closely-related model versions (e.g., as in a “perturbed physics” ensemble) or for the same
    /// model run with slightly different parameterizations (e.g., of cloud physics).  Model versions that are
    /// substantially different from one another should be given a different source_id” (rather than simply assigning a
    pub physics: u32,
    /// Used to distinguish runs conforming to the protocol of a single CMIP6 experiment, but with different variants
    /// of forcing applied.
    pub forcing: u32,
}

impl ToString for VariantLabel {
    fn to_string(&self) -> String {
        format!(
            "r{}i{}p{}f{}",
            self.realization, self.initialization, self.physics, self.forcing
        )
    }
}
