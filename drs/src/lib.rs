#![warn(missing_docs)]
//! This module handles extracting metadata from various DRS path specifications.
//!
//! It presents functions which take in DRS paths and returns objects containing the path given (to facilitate opening
//! the file later if needed) along with an object holding the metadata found in the path. Currently it does not provide
//! functionality to open the files and extract any metadata from the file's attribute data.
//!
//! # Example:
//! ```
//! use camino::Utf8Path;
//! use drs::cmip5::Cmip5;
//!
//! let path = concat!(
//!         "cmip5/output1/NOAA-GFDL/GFDL-CM3/1pctCO2/fx/ocean/fx/r0i0p0/v20120227/gridspec/",
//!         "gridspec_ocean_fx_GFDL-CM3_1pctCO2_r0i0p0.nc"
//!     );
//! let cmip5 = Cmip5::from_esgf_path(path.into()).unwrap();
//! assert_eq!(path, cmip5.path);
//! assert_eq!("NOAA-GFDL", cmip5.metadata.institute);
//! // ...
//! ```
//!
//! This relies heavily on the [`nom`](https://crates.io/crates/nom) crate for parsing the paths which allows for this
//! to be fast with minimal allocations hence the `metadata` objects will reference parts of `path`. Any comparisons
//! which are case insensitive do allocate a temporary string as part of `to_lowercase`.

pub mod cmip5;
pub mod cmip6;
pub mod cordex;
mod parser;
