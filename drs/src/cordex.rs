//! Parsing and validation for files that conform to the CORDEX DRS standard.
//!
//! Source: <https://is-enes-data.github.io/cordex_archive_specifications.pdf>
//! See section 5.1
//!
//! Cordex directory structure:
//! ```text
//! <activity>/<product>/<domain>/<institution>/
//! <gcm_model_name>/<cmip5_experiement_name>/<cmip5_ensemble_member>/
//! <rcm_model_name>/<rcm_version_id>/<frequency>/<variable_name>/[<version>/]
//! ```
//! Cordex file name structure:
//! ```text
//! <variable_name>_<domain>_<gcm_model_name>_
//! <cmip5_experiment_name>_<cmip5_ensemble_member>_
//! <rcm_model_name>_<rcm_version_id>_<frequency>[_<start_time>-<end_time>].nc
//! ```
//!
//! # Exceptions to the Cordex standard:
//! * According to the spec, `activity` and `product` must be `cordex` and `output` respectively. This allows
//!     arbitrary values for these elements.
//! * There is no `version` element in the spec but this allows for an optional `version` at the end of the
//!     path portion (before the filename).
//! * This allows frequency values of `1hr` in addition to the normal values

use std::str::FromStr;

use camino::Utf8Path;
use thiserror::Error;

use crate::{
    cmip5::{EnsembleMember, PartialDateTime},
    parser::parse_cordex,
};

/// Error parsing a cordex style path
#[derive(Debug, Error)]
#[error("error parsing CORDEX path: {reason}")]
pub struct InvalidCordexPathError {
    reason: String,
}

/// Contains the path and metadata for a cordex-style DRS path.
#[derive(Debug)]
pub struct Cordex<'a> {
    /// The path of the file relative to the root directory of the dataset meaning it contains all of and only the
    /// elements relevant to CORDEX.
    pub path: &'a Utf8Path,
    /// The metadata extracted from the path
    pub metadata: PathMetadata<'a>,
}

impl<'a> Cordex<'a> {
    /// Extracts metadata from a CORDEX style file path. It expects that the path will consist of only the parts relevant
    /// to the DRS structure. For example, if a dataset starts at `/foo/data/cordex/output...` then `path` must start
    /// from `cordex/output...`.
    ///
    /// Note:
    /// * This will not verify that the file upholds various cross-field constraints of the spec  (e.g. that `fx`
    ///     frequency has an ensemble value of `r0i0p0`).
    pub fn from_path(path: &'a Utf8Path) -> Result<Self, InvalidCordexPathError> {
        let metadata = match parse_cordex(path.as_str()) {
            Ok((_, metadata)) => metadata,
            Err(e) => {
                return Err(InvalidCordexPathError {
                    reason: e.to_string(),
                })
            }
        };

        Ok(Self { path, metadata })
    }
}

/// Cordex metadata for a file path and name.
///
/// Field documentation is mostly copied from the source document. Fields which have limited sets of value that are
/// updated remotely (e.g. institution) currently do not have those lists checked to ensure values are correct. This
/// may change later. The lists themselves are still linked for reference.
///
/// Where relevant, name in attributes is referring to the key within the netcdf file's metadata for that field.
#[derive(Debug, PartialEq, Eq)]
pub struct PathMetadata<'a> {
    /// Name of the activity for this dataset. In the spec this must be `cordex` but this is not enforced here.
    pub activity: &'a str,
    /// Name of the product of this dataset. In the spec this must be `output` but this is not enforced here.
    pub product: &'a str,
    /// The name assigned to each of the CORDEX regions and includes a flag for resolution.
    ///
    /// Possible values come from the name column(?) of Tables 1 and 2 in the source document.
    ///
    /// Name in attributes: `CORDEX_domain`
    pub domain: &'a str,
    /// identifier for the institution that is responsible for the scientific aspects of the CORDEX simulation (RCM
    /// configuration, experiments ...).
    ///
    /// Updated list of possible values: <http://is-enes-data.github.io/CORDEX_RCMs_ToU.txt>.
    ///
    /// Name in attributes: `institute_id`
    pub institution: &'a str,
    /// Identifier of the driving data. The name consists of an institute identifier and a model identifier. For
    /// reanalysis driven runs these are ECMWF and a name for the reanalysis data (ERAINT). For runs driven by CMIP5
    /// model data these are the associated CMIP5 institute_id and the CMIP5 model_id. The two parts of the name are
    /// separated by a `-` (dash). Note that dashes in either of the two parts are allowed.
    ///
    /// List of possible values: <http://is-enes-data.github.io/GCMModelName.txt>
    ///
    /// Name in attributes: `driving_model_id`
    pub gcm_model_name: &'a str,
    /// Either `"evaluation"` or the value of the CMIP5 experiment_id of the data used.
    ///
    /// Name in attributes: `driving_experiment_name`
    pub cmip5_experiment_name: &'a str,
    /// Identifies the ensemble member of the CMIP5 experiment that produced the forcing data. It has to have the same
    /// value in CORDEX as in CMIP5. For evaluation runs it has to be r1i1p1. Invariant fields (frequency=fx) may have
    /// the value r0i0p0 or that of the corresponding GCMEnsembleMember attribute.
    ///
    /// Name in attributes: `driving_model_ensemble_member`
    pub cmip5_ensemble_member: EnsembleMember,
    /// Identifier of the CORDEX RCM. It consists of the Institution identifier (see above) and a model acronym,
    /// connected by a dash (e.g. DMI-HIRHAM5 or SMHI-RCA4). The CV of the RCMModelName has to be coordinated in the
    /// worldwide CORDEX community. Segments of the model name are allowed to include `-`
    ///
    /// List of possible values: <http://is-enes-data.github.io/CORDEX_RCMs_ToU.txt>
    ///
    /// Name in attributes: `model_id`
    pub rcm_model_name: &'a str,
    /// Identifies reruns with perturbed parameters or smaller RCM release upgrades, i.e. equivalent simulations. Major
    /// upgrades and improvements should be reflected in the RCMModelName.
    ///
    /// Name in attributes: `rcm_version_id`
    pub rcm_version_id: &'a str,
    /// The frequency of the data. See [`Frequency`] for more details.
    pub frequency: Frequency,
    /// The name of the target variable in the NetCDF files. Possible values are found in
    /// <http://is-enes-data.github.io/CORDEX_variables_requirement_table.pdf>
    pub variable_name: &'a str,
    /// The version of the data. Generally either an incrementing number as this dataset is revised or a string indicating
    /// the data when the data was generated.
    ///
    /// This is not in the spec file but exists in real usage.
    pub version: Option<&'a str>,

    /// Indicates the range of data covered by this file in the form `start-end` which maps to `start` as the first
    /// element of the tuple and `end` as the second.
    ///
    /// The format is `YYYY[MM[DD[HH[MM]]]]`, i.e. the year is represented by 4 digits, while the month, day, hour, and
    /// minutes are represented by exactly 2 digits, if they are present at all.
    pub time_range: Option<(PartialDateTime, PartialDateTime)>,
}

/// The output frequency indicator: `3hr`=3 hourly, `6hr`=6 hourly, `day`=daily, `mon`=monthly, `sem`=seasonal, and
/// `fx`=invariant fields.
///
/// This additionally allows values of `1hr` corresponding to 1 hourly data.
///
/// Name in attributes: `frequency`
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Frequency {
    /// Hourly data frequency.
    ///
    /// This is not part of the spec but added because it's widely used.
    Hour1,
    /// 3-hourly frequency
    Hour3,
    /// 6-hourly frequency
    Hour6,
    /// Daily frequency
    Day,
    /// Montly frequency
    Month,
    /// Seasonal frequency meaning the data is per season
    Seasonal,
    /// Fixed frequency meaning the dataset does not change over time
    Fixed,
}

impl ToString for Frequency {
    fn to_string(&self) -> String {
        use Frequency::*;
        let s = match self {
            Hour1 => "1hr",
            Hour3 => "3hr",
            Hour6 => "6hr",
            Day => "day",
            Month => "mon",
            Seasonal => "sem",
            Fixed => "fx",
        };
        s.to_owned()
    }
}

/// Error parsing frequency from string
#[derive(Debug, Error)]
#[error("invalid frequency value {given}")]
pub struct InvalidFrequencyError {
    given: String,
}

impl FromStr for Frequency {
    type Err = InvalidFrequencyError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Frequency::*;
        let s = s.to_lowercase();
        match s.as_str() {
            "1hr" => Ok(Hour1),
            "3hr" => Ok(Hour3),
            "6hr" => Ok(Hour6),
            "day" => Ok(Day),
            "mon" => Ok(Month),
            "sem" => Ok(Seasonal),
            "fx" => Ok(Fixed),
            _ => Err(InvalidFrequencyError { given: s }),
        }
    }
}
