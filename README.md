# Freva Metadata Ingestion

This project will ingest metadata retrieved from the filesystem per DRS specifications and store them in a Solr instance
for use by Freva

# Installation

Downloads are available on the [Releases page](https://gitlab.dkrz.de/freva/freva-ingest/-/releases). Download the
appropriate binary from the latest release for your OS and architecture (currently only x86_64 Linux is built
automatically), unzip the download, and place the binary in your `PATH`.

# Configuration

This expects 2 config files in the project directory for this project as defined by the [directories
crate](https://docs.rs/directories/latest/directories/). A table for each OS is
[here](https://docs.rs/directories/latest/directories/struct.ProjectDirs.html#method.config_dir), for Linux users it
will be `$HOME/.config/freva`. Within the directory it will look for `drs_conf.toml` and `evaluation_system.conf`.
Examples of the config files are available in `./.docker`.

This can also be overridden with a `EVALUATION_SYSTEM_CONFIG_DIR` envvar.

A small example config (reformatted version of the file found in `.docker/drs_config.toml`):

```toml
[cmip5_name]
root_dir = ".docker/data/cmip5"
drs_format = "cmip5"

[observations]
root_dir = ".docker/data/obs"
drs_format = "custom"
parts_dir = [
    "project", "product", "institute", "model", "experiment", "time_frequency", "realm",
    "cmor_table", "ensemble", "version", "variable"
]
parts_file_name = ["variable", "time_frequency", "experiment", "level", "version", "time"]
parts_time = "start_time-end_time"
[observations.defaults]
project = "observations"

```

## Custom Dataset Definitions

Custom dataset definitions require the additional values `parts_dir` and `parts_file_name`, `defaults` is optional.

- `parts_dir` defines which sections of the path (`/` separated) corresponds to which metadata values,
  note the lack of a `filename` element at the end which is assumed to be there in a valid DRS path.
- `parts_file_name` defines the same for the file name (`_` separated) and generally assumes that the file ends with
  `.nc` as per the DRS specifications.
- `defaults` is used to define constant values for missing elements. For example if you have a `cmip5` structure but
  without the `cmip5` that should be at the start of the path, you could create a custom structure and set
  `project=cmip5` in `defaults`.

The required keys are `project`, `product`, `institute`, `model`, `experiment`, `time_frequency`, `realm`, `cmor_table`,
`ensemble`, and `variable`. `version` is used and optional, if it's missing the ingested files will simply have a
version value. `time` is also optional, see Time Range below for more details. All other values will be ignored which
can be useful to document what an unused element is. Values can appear multiple times and in such cases the value that
is ultimately used is the last instance found in the order of `defaults`, `parts_dir`, `parts_file_name` (and within
each, the last instance will take priority).

For example, given a (not quite valid) config like:

```toml
[foo]
root_dir = "..."
drs_format = "custom"
parts_dir = [
    "project", "product", "variable", "model", "time_frequency", "realm", "variable"
]
parts_file_name = ["variable", "time_frequency", "experiment", "variable", "version"]
[foo.defaults]
variable = "variable0"
```

Note that `variable` is there five times, first in `defaults`, twice in `parts_dirs`, and twice in `parts_file_name`.

Given a path `proj/prod/variable1/mod/freq/realm/variable2/variable3_freq_exp_variable4_v1.nc`, the value for
`variable` that will be ingested is `variable4` as it is the last one found.

### Time Range

`time` is the only key that will require a certain structure in its data. A valid time range must look like a
CMIP5 time range, i.e. `start-end` where the section before the `-` is the start of the time range covered by the data
and the section after is the end. Each individual time must be of the form `YYYY[MM[DD[HH[mm[ss]]]]]` where `YYYY` means
the 4 digit year, `MM` is the 2 digit month `DD` is digit 2 day, `HH` is 2 digit hour (24 hour, not 12), `mm` is 2 digit
minute, and `ss` is 2 digit seconds. This means that each time must have at least a year value, each additional value is
optional but requires all those before e.g. that you can't have a time that specifies just year and hour, you must give
year, month, day, and hour. So a valid time might look like `20100405101112` which would mean `2010-04-05` at 10:11 and
12 seconds.

If `time` is not present, all files will be given the maximum range allowed by this schema: year `0` to year `9999`.

# Usage

## Ingestion

Running just `freva-ingest ingest` will ingest all files under all of the defined datasets.

`freva-ingest ingest --data-dir <path>` can be used to ingest just the files underneath that directory. This
will require that `<path>` is a directory within one of the defined datasets and can be set to a directory deep within
a defined dataset to import just the files under that directory.

## Deletion

`freva-ingest delete <path>` will delete files within `<path>` from solr. It will ask for confirmation before deleting
anything and the `-y` flag can be used in scripts to skip that verification step. `<path>` is a required argument so to
delete everything, use `/`.

## Completion

This provides completion via `freva-ingest completion <shell>` which should either be sourced directly or put into a
file depending on how your shell handles completion. Completions are handled through `clap`'s automatic generation.

### Bash

Add the following to your `.bashrc`.

```bash
source <(freva-ingest completion bash)
```

### Zsh

Zsh requires more setup than Bash. You need to decide where to put the completion file, either in a directory already in
`echo $fpath` or create one of your own and add it to `fpath`. This example will assume you want them in
`~/.zsh/completion`.

Run `freva-ingest completion zsh >! ~/.zsh/completion/_freva-ingest`. `>!` is used to overwrite the file if it already
exists which is useful because the file will need to be manually recreated when updated. Then add the following to your
`.zshrc`.

```zsh
fpath=(~/.zsh/completion $fpath)
compinit
```
