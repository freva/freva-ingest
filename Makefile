
build:
	cargo build

lint:
	cargo fmt --check
	cargo clippy -- -D warnings

docs:
	cargo doc --open --lib --no-deps

cli-docs:
	cargo doc --open --bin=cli --no-deps

run-example-data:
	cargo run --bin freva-ingest -- ingest

cmip6-data:
	cargo run --bin cmip6-creator

# sadly necessary to get this compiling for Linux on Mac thanks to ring
linux-build:
	docker run \
	-v $(PWD):/workspace \
	-w /workspace \
	rust:latest \
	/bin/bash -c " \
		apt-get update -y && apt-get install -y \
			libssl-dev cmake && \
		cargo build --release \
	"

musl-build:
	docker run \
	-v $(PWD):/workspace \
	-w /workspace \
	rust:latest \
	/bin/bash -c " \
		apt-get update -y && apt-get install -y \
			musl-tools libssl-dev cmake && \
		rustup target add x86_64-unknown-linux-musl && \
		cargo build --release --target x86_64-unknown-linux-musl \
	"
