use std::path::PathBuf;

use camino::Utf8PathBuf;
use clap::builder::ArgAction::Count;
use clap::{Parser, Subcommand};

#[derive(Debug, Parser)]
#[clap(author, version, about)]
pub struct Cli {
    #[clap(
        short,
        action(Count),
        help = "Log level to use, defaults to warning (0) up to trace (3+)."
    )]
    pub verbosity: u8,

    #[clap(
        long,
        action,
        env = "EVALUATION_SYSTEM_CONFIG_DIR",
        help = concat!("Directory with freva config files. If not set, defaults to an OS specific default",
            " e.g. on Linux ~/.config/freva")
    )]
    pub config_dir: Option<PathBuf>,

    #[clap(subcommand)]
    pub cmd: Command,
}

#[derive(Debug, Subcommand)]
pub enum Command {
    /// Ingest DRS-like file paths into Solr.
    Ingest(IngestOpts),
    /// Delete file metadata from Solr.
    Delete(DeleteOpts),
    /// Creates a completion script for the given shell and prints it to stdout.
    Completion(CompletionOpts),
}

#[derive(Debug, Parser)]
pub struct DeleteOpts {
    #[clap(action,
        help = concat!("The path to delete from Solr. This will delete all files with a path that starts with what ",
            "is given here.")
    )]
    pub path: Utf8PathBuf,

    #[clap(
        short = 'y',
        action,
        help = "Skip confirmation before sending delete query."
    )]
    pub yes: bool,
}

#[derive(Debug, Parser)]
pub struct IngestOpts {
    #[clap(
        long,
        action,
        help = concat!("Directory to ingest. If not present, all configured datasets are ingested.",
            " Can be set to a directory within a dataset to only import data within that directory using the ",
            "enclosing dataset for its configuration.")
    )]
    // TODO: if clap ever supports built-in argument completion, this should be changed to query the paths in solr
    // instead of the filesystem since you're probably deleting from solr because the files aren't on the filesystem
    // anymore.
    // Tracking issue: https://github.com/clap-rs/clap/issues/3166
    pub data_dir: Option<PathBuf>,

    #[clap(
        long,
        action,
        help = "Size of batches to send to solr",
        default_value = "1000"
    )]
    pub batch_size: usize,
}

#[derive(Debug, Parser)]
pub struct CompletionOpts {
    #[clap(action, help = "shell to generate completions for")]
    pub shell: clap_complete::Shell,
}
