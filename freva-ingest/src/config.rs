use std::fs::File;
use std::io::Read;
use std::path::Path;

use anyhow::anyhow;
use anyhow::{Context, Result};
use configparser::ini::Ini;

use freva::drs::metadata::ConfigBuilder;
use log::debug;

#[derive(Debug)]
pub struct Config {
    pub solr: SolrConfig,
}

#[derive(Debug)]
pub struct SolrConfig {
    pub host: String,
    pub port: Option<u16>,
    pub core: String,
}

const CONFIG_NAME: &str = "evaluation_system.conf";
const DRS_CONFIG_NAME: &str = "drs_config.toml";

const EVAL_SYSTEM_KEY: &str = "evaluation_system";

pub fn config<P: AsRef<Path>>(config_dir: P) -> Result<Config> {
    let config_file = config_dir.as_ref().join(CONFIG_NAME);
    debug!("loading freva config from {}", config_file.display());

    let mut config = Ini::new();
    let result = config.load(config_file);
    if let Err(e) = result {
        return Err(anyhow!(e));
    }

    let mut solr = SolrConfig {
        host: "".to_owned(),
        port: None,
        core: "".to_owned(),
    };

    match config.get(EVAL_SYSTEM_KEY, "solr.host") {
        Some(h) => solr.host = h,
        None => return Err(anyhow!("freva config missing required solr.host")),
    };
    match config.get(EVAL_SYSTEM_KEY, "solr.core") {
        Some(c) => solr.core = c,
        None => return Err(anyhow!("freva config missing required solr.core")),
    };
    if let Some(port_str) = config.get(EVAL_SYSTEM_KEY, "solr.port") {
        solr.port = match port_str.parse::<u16>() {
            Ok(p) => Some(p),
            Err(e) => return Err(anyhow!("freva config solr.port value is invalid: {e}")),
        };
    }

    Ok(Config { solr })
}

pub fn drs_config<P: AsRef<Path>>(config_dir: P) -> Result<freva::drs::Config> {
    let config_file = config_dir.as_ref().join(DRS_CONFIG_NAME);
    debug!("loading drs config from {}", config_file.display());

    let mut config_data = String::new();
    File::open(config_file)?.read_to_string(&mut config_data)?;

    let builder: ConfigBuilder = toml::from_str(&config_data)?;
    builder.build().context("error building DRS config")
}
