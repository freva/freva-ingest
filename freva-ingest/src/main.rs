mod cli;
mod commands;
mod config;

use std::io;

use anyhow::{Context, Result};
use clap::{IntoApp, StructOpt};
use clap_complete::generate;
use cli::{Cli, Command};
use config::{config, drs_config};
use directories::ProjectDirs;
use tracing::Level;

const DOMAIN: &str = "de";
const ORGANIZATION: &str = "DKRZ";
const APP_NAME: &str = "freva";

#[tokio::main]
async fn main() -> Result<()> {
    let args = Cli::parse();

    tracing_subscriber::fmt()
        .with_max_level(log_level(args.verbosity))
        .init();

    let config_dir = match args.config_dir {
        Some(c) => c,
        None => ProjectDirs::from(DOMAIN, ORGANIZATION, APP_NAME)
            .context("no config dir given and no valid home directory found")?
            .config_dir()
            .to_owned(),
    };

    let conf = config(&config_dir)?;
    let drs_conf = drs_config(&config_dir)?;

    match args.cmd {
        Command::Ingest(opts) => commands::ingest(&opts, &conf, &drs_conf).await,
        Command::Delete(opts) => commands::delete(&opts, &conf).await,
        Command::Completion(opts) => {
            generate(
                opts.shell,
                &mut Cli::command(),
                env!("CARGO_BIN_NAME"),
                &mut io::stdout(),
            );
            Ok(())
        }
    }
}

fn log_level(verbosity: u8) -> Level {
    match verbosity {
        0 => Level::WARN,
        1 => Level::INFO,
        2 => Level::DEBUG,
        _ => Level::TRACE,
    }
}
