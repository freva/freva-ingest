use std::collections::HashMap;
use std::env::current_dir;
use std::io::{stdin, stdout, Write};

use anyhow::{bail, Context, Result};
use camino::Utf8PathBuf;
use freva::drs::Config as DrsConfig;
use freva::solr::{escape_query, Solr};
use freva::DEFAULT_LATEST_COLLECTION;

use crate::cli::{DeleteOpts, IngestOpts};
use crate::config::Config;

pub async fn ingest(opts: &IngestOpts, conf: &Config, drs_conf: &DrsConfig) -> Result<()> {
    let solr = Solr::new(conf.solr.host.clone(), conf.solr.port, None)?;

    let reports = freva::drs::ingest(
        &solr,
        drs_conf,
        &opts.data_dir.as_deref(),
        opts.batch_size,
        Some(&conf.solr.core),
        None,
    )
    .await
    .context("error ingesting data to solr")?;

    for r in reports.datasets.iter() {
        let time = humantime::format_duration(r.time);
        println!(
            "[{}]\n\
            sent    {}\n\
            skipped {}\n\
            took    {}\n\
        ",
            r.dataset.name(),
            r.sent,
            r.skipped,
            time,
        );
    }

    let time = humantime::format_duration(reports.total_time);
    println!("total time took {}", time);

    Ok(())
}

pub async fn delete(opts: &DeleteOpts, conf: &Config) -> Result<()> {
    let solr = Solr::new(conf.solr.host.clone(), conf.solr.port, None)?;

    // ensure the path is fully qualified but don't check if the requested directory exists
    let path = if !opts.path.has_root() {
        let root = current_dir()?;
        Utf8PathBuf::try_from(root.join(&opts.path))?
    } else {
        opts.path.to_owned()
    };

    let facets = {
        let mut m = HashMap::new();
        m.insert(
            escape_query("file"),
            // {}* so that we can put in just a parent directory and have all children deleted
            format!("{}*", escape_query(path.as_str())),
        );
        m
    };
    let docs = solr.search(&conf.solr.core, 0, &facets).await?;
    if docs.response.num_found == 0 {
        bail!("No documents found matching the given path");
    }

    println!("Found {} documents to delete", docs.response.num_found);

    // I think the more verbose form is more readable
    #[allow(clippy::collapsible_else_if)]
    if opts.yes {
        println!("Skipping confirmation and deleting");
    } else {
        if !prompt("Are you sure you want to continue? This is not reversible. [y/N]")? {
            bail!("Stopping.");
        }
    }

    let (files, latest) = tokio::join!(
        solr.delete(&conf.solr.core, &facets),
        solr.delete(DEFAULT_LATEST_COLLECTION, &facets)
    );
    let (_, _) = (files?, latest?);

    Ok(())
}

fn prompt(msg: &str) -> Result<bool> {
    print!("{} ", msg);
    stdout().flush()?;
    let mut input = String::new();
    stdin().read_line(&mut input)?;
    match input.trim() {
        "y" | "Y" => Ok(true),
        _ => Ok(false),
    }
}
