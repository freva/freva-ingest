#![warn(missing_docs)]
//! Freva

pub mod drs;
pub mod solr;

/// Default collection name in Solr for all files
pub const DEFAULT_FILES_COLLECTION: &str = "files";
/// Default collection name in Solr for the latest versions of all versioned files
pub const DEFAULT_LATEST_COLLECTION: &str = "latest";
