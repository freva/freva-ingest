//! Wrapper around the Solr JSON API

use std::collections::HashMap;
use std::fmt::Write;
use std::io;

use reqwest::{Client, Url};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tracing::{debug, trace};

const DEFAULT_PORT: u16 = 8983;
const DEFAULT_PROTOCOL: &str = "http";

// See "Escaping Special Characters" in
// https://lucene.apache.org/core/9_2_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html
const SPECIAL_CHARS: [char; 19] = [
    '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '"', '~', '*', '?', ':', '\\', '/',
];

/// Errors
#[derive(Debug, Error)]
pub enum SolrError {
    /// Error in connecting to Solr
    #[error("error connecting to Solr: {0}")]
    Network(#[from] io::Error),

    /// Error in requests sent to Solr
    #[error("error in request to Solr: {0}")]
    RequestFailed(#[from] reqwest::Error),

    /// Some element of the url was not valid. This can happen for 2 reasons:
    ///     1. Invalid Solr instance information (e.g. wrong protocol)
    ///     2. Solr collection name creates an invalid url
    #[error("invalid url: {0}")]
    ParseUrlError(#[from] url::ParseError),

    /// Solr API has returned a non-successful status code
    #[error("Solr returned non-ok status: {status}\n{body}")]
    ResponseNotOk {
        /// status that was received
        status: reqwest::StatusCode,
        // TODO: this can likely be better described than just taking a string of the json response
        /// body of the response with information about the failure
        body: String,
    },
}

/// Wrapper around the Solr JSON API
///
/// Relevant docs:
/// * <https://solr.apache.org/guide/solr/latest/query-guide/json-request-api.html>
/// * <https://solr.apache.org/guide/solr/latest/indexing-guide/indexing-with-update-handlers.html>
#[derive(Debug, Clone)]
pub struct Solr {
    base_url: Url,
    client: Client,
}

impl Solr {
    /// Constructs a new `Solr` pointing to the indicated solr instance.
    /// If `port` is `None`, Solr's default port is used.
    pub fn new(
        host: String,
        port: Option<u16>,
        protocol: Option<String>,
    ) -> Result<Self, SolrError> {
        let port = port.unwrap_or(DEFAULT_PORT);
        let protocol = protocol.unwrap_or_else(|| DEFAULT_PROTOCOL.to_owned());

        let url = Url::parse(&format!("{}://{}:{}", protocol, host, port))?;
        Ok(Self {
            base_url: url,
            client: Client::new(),
        })
    }

    fn url(&self, collection: &str, method: &str) -> Result<Url, url::ParseError> {
        self.base_url
            .join(&format!("solr/{}/{}", collection, method))
    }

    /// Queries Solr `collection` for documents using `start` as the beginning offset to use.  Allows filtering via
    /// `facets`. *Facets, both keys and values, are expected to be already be escaped when calling this method via
    /// `escape_query`*.
    pub async fn search(
        &self,
        collection: &str,
        start: usize,
        facets: &HashMap<String, String>,
    ) -> Result<SearchResponse, SolrError> {
        let url = self.url(collection, "select")?;

        debug!("sending search request to {}", url);

        let query = build_query_str(facets);
        // convert start to a string representation outside of HashMap::from for lifetime reasons
        let start_str = format!("{}", start);

        let parameters = HashMap::from([("wt", "json"), ("q", &query), ("start", &start_str)]);

        let req = self.client.get(url).query(&parameters);
        trace!("sending request:\n{:#?}", req);

        let res = req.send().await?;

        trace!("received response:\n{:#?}", res);
        if res.status() != reqwest::StatusCode::OK {
            Err(SolrError::ResponseNotOk {
                status: res.status(),
                body: res.json().await?,
            })
        } else {
            let response_body: SearchResponse = res.json().await?;
            Ok(response_body)
        }
    }

    /// Uploads `documents` into `collection` in Solr.
    pub async fn update(
        &self,
        collection: &str,
        documents: &[HashMap<&str, String>],
    ) -> Result<(), SolrError> {
        let url = self.url(collection, "update/json/docs")?;

        debug!("sending update request to {}", url);

        let params = HashMap::from([
            ("overwrite", "true"),
            ("commit", "true"), // commit the data immediately
        ]);
        let req = self
            .client
            .post(url)
            .header(reqwest::header::CONTENT_TYPE, "application/json")
            .query(&params)
            .json(&documents);

        trace!("sending request:\n{:#?}", req);

        let res = req.send().await?;
        trace!("received response:\n{:#?}", res);

        if res.status() != reqwest::StatusCode::OK {
            Err(SolrError::ResponseNotOk {
                status: res.status(),
                body: res.text().await?,
            })
        } else {
            Ok(())
        }
    }

    /// Deletes the files which match the given facets. *Facets, both keys and values, are expected to be already be
    /// escaped when calling this method via `escape_query`*.
    pub async fn delete(
        &self,
        collection: &str,
        facets: &HashMap<String, String>,
    ) -> Result<(), SolrError> {
        let url = self.url(collection, "update")?;

        debug!("sending delete request to {}", url);

        let params = &[("overwrite", "true"), ("commit", "true")];
        let body = DeleteRequestBody {
            delete: Query {
                query: &build_query_str(facets),
            },
        };

        let req = self
            .client
            .post(url)
            .header(reqwest::header::CONTENT_TYPE, "application/json")
            .query(&params)
            .json(&body);

        trace!("sending request:\n{:#?}", req);

        let res = req.send().await?;
        trace!("received response:\n{:#?}", res);

        if res.status() != reqwest::StatusCode::OK {
            Err(SolrError::ResponseNotOk {
                status: res.status(),
                body: res.text().await?,
            })
        } else {
            Ok(())
        }
    }
}

/// Response body from a `/search` request
#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SearchResponse {
    /// Header
    pub response_header: ResponseHeader,
    /// Data
    pub response: SearchResponseBody,
}

/// The document data of the response
#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SearchResponseBody {
    /// number of documents found that match this query
    pub num_found: usize,
    /// the starting index of this page of results
    pub start: i32,
    /// the documents in this page of results
    pub docs: Vec<FileDocument>,
}

/// Header of a solr response
#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ResponseHeader {
    /// HTTP status code
    pub status: i32,
    /// Query parameters of the sent request
    pub params: HashMap<String, String>,
}

/// File document stored in Solr.
/// TODO: maybe this should be pulled out of here and defined by the caller?
#[derive(Debug, Deserialize)]
pub struct FileDocument {
    /// File's path
    pub file: String,
    /// When this document was inserted into Solr
    pub creation_time: String,
}

#[derive(Serialize)]
struct DeleteRequestBody<'a> {
    delete: Query<'a>,
}

#[derive(Serialize)]
struct Query<'a> {
    query: &'a str,
}

/// Escapes special characters in Solr query elements.
///
/// Examples:
/// ```rust
/// use freva::solr::escape_query;
///
/// assert_eq!(escape_query("/foo/bar"), "\\/foo\\/bar");
/// assert_eq!(escape_query("foo!bar"), "foo\\!bar");
/// assert_eq!(escape_query("foo+bar-baz"), "foo\\+bar\\-baz");
/// assert_eq!(escape_query("q:foo*&&ba?z"), "q\\:foo\\*\\&\\&ba\\?z");
/// ```
///
/// See also [the Lucene documentation][1].
///
/// [1]: https://lucene.apache.org/core/9_2_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#Escaping_Special_Characters
pub fn escape_query(s: &str) -> String {
    // This isn't ideal, a better API would be more like SQL-style parameterization e.g.
    // `query("project=?*\ninstitution=?", "foo", "bar")` but this isn't intended to be a full client for Solr right
    // now. If we expand the scope of this beyond that, we may want to revisit this.
    let mut escaped = String::new();
    for c in s.chars() {
        if SPECIAL_CHARS.contains(&c) {
            escaped.push('\\');
        }
        escaped.push(c);
    }
    escaped
}

fn build_query_str(facets: &HashMap<String, String>) -> String {
    let mut query_str = String::new();
    for (k, v) in facets.iter() {
        // TODO: This uses writeln to separate the query terms by newlines. I'm not actually sure this is how to
        // separate different query operands.

        // written this way to satisfy clippy, ignored result should be fine
        // see: https://rust-lang.github.io/rust-clippy/master/index.html#format_push_string
        let _ = writeln!(&mut query_str, "{}:{}", k, v);
    }
    query_str
}
