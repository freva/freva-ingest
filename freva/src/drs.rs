//! Handles ingestion of metadata from the filesystem that conforms to the DRS spec
//!
//! DRS paper:
//! <https://pcmdi.llnl.gov/mips/cmip5/docs/cmip5_data_reference_syntax.pdf>

mod ingest;
pub mod metadata;
mod path;
mod search;

pub use ingest::ingest;
pub use search::search;

pub use metadata::{Config, Dataset, Error, Metadata};
