//! Implementation of the DRS specification for extracting metadata information from file paths.

mod config_builder;
pub mod custom;

use core::fmt;
use std::{
    collections::HashMap,
    fmt::Formatter,
    fmt::Write as _,
    io,
    path::{Path, StripPrefixError},
};

use camino::{Utf8Path, Utf8PathBuf};
use drs::{
    cmip5::{Cmip5, PartialDateTime},
    cmip6::{Cmip6, MIP_ERA},
    cordex::Cordex,
};
use lazy_static::lazy_static;
use netcdf::AttrValue;
use thiserror::Error;
use tokio::sync::Mutex;
use tracing::{debug, error, trace};

pub use config_builder::ConfigBuilder;
use custom::Custom;

use crate::drs::path::absolute;

/// Errors that can be returned by DRS functions
#[derive(Debug, Error)]
pub enum Error {
    /// Error canonicalizing paths.
    /// This probably means that the current directory doesn't exist or the user does not have permission to access it
    #[error("error getting absolute path: {0}")]
    AbsolutePath(#[from] io::Error),

    /// Error where path is not valid for a given [`Dataset`]
    #[error("Not a valid DRS file: {0}")]
    InvalidPath(String),

    /// Path did not start with the relevant dataset's root_dir
    #[error("Incorrect prefix for dataset: {0}")]
    InvalidPrefix(#[from] StripPrefixError),

    /// Error that happens when given a path that isn't valid utf 8
    #[error("path is not utf-8")]
    NonUtf8Path(#[from] camino::FromPathBufError),

    /// Error getting netcdf attributes
    #[error(transparent)]
    ExtractMetadata(#[from] ExtractMetadataError),

    /// Path does not follow cmor standard
    #[error(transparent)]
    InvalidCmorPath(#[from] drs::cmip5::InvalidCmorPathError),

    /// Path does not follow esgf standard
    #[error(transparent)]
    InvalidEsgfPath(#[from] drs::cmip5::InvalidEsgfPathError),

    /// Path does not follow cmip6's specification
    #[error(transparent)]
    InvalidCmip6Path(#[from] drs::cmip6::InvalidCmip6PathError),

    /// Path does not follow cordex's specification
    #[error(transparent)]
    InvalidCordexPath(#[from] drs::cordex::InvalidCordexPathError),

    /// Path does not follow defined custom specification
    #[error(transparent)]
    InvalidCustomPath(#[from] custom::Error),
}

/// Which specification a DRS path follows and, if custom, additional required information to parse it
#[derive(Debug, Clone)]
pub enum Specification {
    /// Path follows the CMIP5 CMOR format
    Cmor,
    /// Path follows the current CMIP5 ESGF format
    Cmip5,
    /// Path follows the CMIP6 format
    Cmip6,
    /// Path follows The CORDEX format
    Cordex,
    /// Path does not follow a support DRS specification but one that's defined in its [`custom::ComponentConfig`]
    Custom(custom::ComponentConfig),
}

impl Specification {
    /// Dispatches a _relative_ path to the correct module to process the path based on which type of DRS spec it
    /// follows.
    ///
    /// Relative path here means it starts from the beginning of the DRS portion of the path. For example if a
    /// dataset's `root_dir` is `/foo/bar` and a file within that dataset is `/foo/bar/cmip5/some/other/things.nc`
    /// then `path` here would be `cmip5/some/other/things.nc`.
    async fn metadata_from_path(
        &self,
        root_dir: &Utf8Path,
        path: &Utf8Path,
    ) -> Result<Metadata, Error> {
        use drs::cmip5::Cmip5 as drs_cmip5;
        use drs::cmip6::Cmip6 as drs_cmip6;
        use drs::cordex::Cordex as drs_cordex;
        use Specification::*;
        let m: Metadata = match self {
            Cmor => drs_cmip5::from_cmor_path(path)?.into(),
            Cmip5 => drs_cmip5::from_esgf_path(path)?.into(),
            Cmip6 => {
                let cmip = drs_cmip6::from_path(path)?;
                metadata_from_cmip6(cmip, root_dir).await?
            }
            Cordex => drs_cordex::from_path(path)?.into(),
            Custom(c) => c.metadata_from_path(path)?.into(),
        };
        Ok(m)
    }
}

impl fmt::Display for Specification {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use Specification::*;
        let s = match self {
            Cmor => "CMOR",
            Cmip5 => "CMIP5",
            Cmip6 => "CMIP6",
            Cordex => "CORDEX",
            Custom(_) => "Custom",
        };
        write!(f, "{}", s)
    }
}

/// A DRS dataset which describes the DRS specification used by the data within its `root_dir` and the human readable
/// name attached to associate it with an entry in the config file
#[derive(Debug, Clone)]
pub struct Dataset {
    name: String,
    root_dir: Utf8PathBuf,
    drs_format: Specification,
}

impl Dataset {
    /// Gets the fully qualified root directory for this set of DRS files.
    pub fn root(&self) -> &Utf8Path {
        &self.root_dir
    }

    /// Processes a path according to this dataset's `drs_format` and returns the normalized metadata
    pub async fn metadata_from_path(&self, path: &Utf8Path) -> Result<Metadata, Error> {
        // in the normal flow of this code as currently written, it's shouldn't be possible for stripping the prefix to
        // cause an error
        let path = path.strip_prefix(&self.root_dir)?;

        let mut metadata = self
            .drs_format
            .metadata_from_path(&self.root_dir, path)
            .await?;
        // put the root back into the path so this contains the entire fully qualified path
        metadata.path = self.root_dir.join(metadata.path);
        Ok(metadata)
    }

    /// Get the human readable name
    pub fn name(&self) -> &str {
        &self.name
    }
}

/// Map of DRS Activity name to [`Dataset`].
/// Used to validate paths against the known DRS datasets and construct [`Metadata`] objects.
#[derive(Debug)]
pub struct Config {
    datasets: Vec<Dataset>,
}

impl Config {
    /// Creates a [`Metadata`] from the given path based on the available dataset
    pub async fn metadata_from_path<P: AsRef<Path>>(&self, path: &P) -> Result<Metadata, Error> {
        let path = absolute(path.as_ref())?;
        let path = Utf8PathBuf::try_from(path)?;

        let dataset = self
            .dataset_from_path(&path)
            .ok_or_else(|| Error::InvalidPath("no matching dataset found".to_owned()))?;
        debug!("found dataset {} for path \n{}", dataset.name(), path);

        dataset.metadata_from_path(&path).await
    }

    /// Returns an iterator over the datasets available in the config.
    pub fn datasets(&self) -> impl Iterator<Item = &Dataset> {
        self.datasets.iter()
    }

    fn dataset_from_path(&self, path: &Utf8Path) -> Option<&Dataset> {
        self.datasets.iter().find(|&s| s.root().starts_with(path))
    }
}

/// [`Metadata`] is the normalized form of DRS metadata that Freva uses which has the same fields as [`Cmip5`] but with
/// no restrictions on what isn't a valid value e.g. `frequency` is not limited to the valid CMIP5 frequency values.
/// This allows us to map the different standards into something with CMIP5 keys though not necessarily with their
/// values.
#[derive(Debug)]
pub struct Metadata {
    // I think it may be possible to avoid taking ownership here to reduce allocations but it's the simplest spot to
    // have this happen for now
    path: Utf8PathBuf,
    activity: String,
    product: String,
    institute: String,
    model: String,
    experiment: String,
    frequency: String,
    modeling_realm: String,
    mip_table: String,
    ensemble: String,
    variable: String,
    version: Option<String>,
    time_range: Option<TimeRange>,
}

impl Metadata {
    /// Gets the path relative path for this file
    pub fn path(&self) -> &Utf8Path {
        &self.path
    }

    /// Constructs a CMIP5-like identifier for the dataset including the version. If there is no version, then `None`
    /// is returned.
    ///
    /// This is CMIP5-like in that the CMIP5 spec doesn't include `variable` which this does.
    pub fn to_identifier(&self) -> Option<String> {
        if self.version.is_none() {
            None
        } else {
            Some(
                vec![
                    self.activity.as_str(),
                    self.product.as_str(),
                    self.institute.as_str(),
                    self.model.as_str(),
                    self.experiment.as_str(),
                    self.frequency.as_str(),
                    self.modeling_realm.as_str(),
                    self.mip_table.as_str(),
                    self.ensemble.as_str(),
                    self.version().unwrap().as_str(),
                    self.variable.as_str(),
                ]
                .join("."),
            )
        }
    }

    /// Constructs a CMIP5-like identifier without version info.
    ///
    /// Like [`Self::to_identifier`], this differs slightly from CMIP5 by including `variable`.
    pub fn to_unversioned_identifier(&self) -> String {
        vec![
            self.activity.as_str(),
            self.product.as_str(),
            self.institute.as_str(),
            self.model.as_str(),
            self.experiment.as_str(),
            self.frequency.as_str(),
            self.modeling_realm.as_str(),
            self.mip_table.as_str(),
            self.ensemble.as_str(),
            self.variable.as_str(),
        ]
        .join(".")
    }

    /// Getter for file version string
    pub fn version(&self) -> Option<&String> {
        self.version.as_ref()
    }

    /// Creates a map of the metadata information we want to send to solr
    pub fn to_solr_map(&self) -> HashMap<&str, String> {
        let mut m = HashMap::new();
        m.insert("file", self.path.to_string());
        m.insert("project", self.activity.to_owned());
        m.insert("product", self.product.to_owned());
        m.insert("institute", self.institute.to_owned());
        m.insert("model", self.model.to_owned());
        m.insert("experiment", self.experiment.to_owned());
        m.insert("time_frequency", self.frequency.to_string());
        m.insert("realm", self.modeling_realm.to_string());
        m.insert("cmor_table", self.mip_table.to_owned());
        m.insert("ensemble", self.ensemble.to_string());
        m.insert("variable", self.variable.to_string());
        if let Some(v) = &self.version {
            m.insert("version", v.clone());
        }
        let time_range = match &self.time_range {
            Some(r) => r.to_solr_string(),
            None => TimeRange::fixed().to_solr_string(),
        };
        m.insert("time", time_range);

        m
    }
}

impl<'a> From<Cmip5<'a>> for Metadata {
    fn from(cmip5: Cmip5<'a>) -> Self {
        let m = &cmip5.metadata;
        let time_range = m.temporal_subset.as_ref().map(|t| TimeRange {
            start_time: t.start.clone(),
            end_time: t.end.clone(),
        });
        Self {
            path: cmip5.path.to_owned(),
            activity: m.activity.to_owned(),
            product: m.product.to_owned(),
            institute: m.institute.to_owned(),
            model: m.model.to_owned(),
            experiment: m.experiment.to_owned(),
            frequency: m.frequency.to_string(),
            modeling_realm: m.modeling_realm.to_string(),
            mip_table: m.mip_table.to_owned(),
            ensemble: m.ensemble.to_string(),
            variable: m.variable.to_owned(),
            version: m.version.map(|v| v.to_owned()),
            time_range,
        }
    }
}

impl<'a> From<Cordex<'a>> for Metadata {
    fn from(cordex: Cordex<'a>) -> Self {
        let m = &cordex.metadata;
        let model = format!(
            "{}-{}-{}",
            m.gcm_model_name, m.rcm_model_name, m.rcm_version_id
        );
        let mip_table = "atmos".to_owned(); // all cordex data is atmos

        let time_range = m.time_range.as_ref().map(|(s, e)| TimeRange {
            start_time: s.clone(),
            end_time: e.clone(),
        });
        Self {
            path: cordex.path.to_owned(),
            activity: m.activity.to_owned(),
            product: m.domain.to_owned(),
            institute: m.institution.to_owned(),
            model,
            experiment: m.cmip5_experiment_name.to_owned(),
            frequency: m.frequency.to_string(),
            modeling_realm: mip_table.clone(),
            variable: m.variable_name.to_owned(),
            ensemble: m.cmip5_ensemble_member.to_string(),
            mip_table,
            version: m.version.map(|v| v.to_owned()),
            time_range,
        }
    }
}

impl<'a> From<Custom<'a>> for Metadata {
    fn from(custom: Custom<'a>) -> Self {
        Self {
            path: custom.path.to_owned(),
            activity: custom.activity.to_owned(),
            product: custom.product.to_owned(),
            institute: custom.institute.to_owned(),
            model: custom.model.to_owned(),
            experiment: custom.experiment.to_owned(),
            frequency: custom.frequency.to_owned(),
            modeling_realm: custom.modeling_realm.to_owned(),
            mip_table: custom.mip_table.to_owned(),
            ensemble: custom.ensemble.to_owned(),
            variable: custom.variable.to_owned(),
            version: custom.version.map(|v| v.to_owned()),
            time_range: custom.time_range,
        }
    }
}

/// Error getting netcdf attributes from a file
#[derive(Debug, Error)]
pub enum ExtractMetadataError {
    /// Error occurred while opening the file. It's not entirely clear which exact error variants are possible when
    /// opening a netcdf file.
    #[error("error extracting necessary metadata from netcdf file")]
    OpenFileError(#[from] netcdf::error::Error),

    /// Given attribute key was not found
    #[error("missing attribute {0}")]
    MissingAttribute(String),

    /// Attribute value for given key was not the expected data type. The type names are meant to be human-readable for
    /// debugging so may not exactly match existing types in code.
    #[error("invalid attribute data for {key} expected {expected} found {found}")]
    InvalidAttributeData {
        /// Attribute key
        key: String,
        /// The name type that was expected (arbitrary string)
        expected: String,
        /// The name type that was found (arbitrary string)
        found: String,
    },
}

lazy_static! {
    /// Holds a mapping of table_id => (frequency, realm) which are the values that we need that cmip6 doesn't carry
    /// in its path data. We can get this information from the netcdf header attributes available in the files
    /// themselves but opening them to inspect is extremely slow. So we build this cache as we process any cmip6 files
    /// because any table_id should always have the same frequency and realm values.
    ///
    /// I'm not sure if this is a property of CMIP6 or just a coincidence but seems to hold for all current possible
    /// values in the controlled vocabulary. See [here](https://github.com/PCMDI/cmip6-cmor-tables) for how table_id
    /// determines frequency and realm.
    ///
    /// This is done as a global cache right now just because performance shouldn't be a large concern as we expect far
    /// more reads than writes and this implementation is simpler than e.g. having the [`drs`] crate load and hold the
    /// mapping values ahead of time.
    static ref CMIP6_ATTR_CACHE: Mutex<HashMap<String, (String, String)>> =
        Mutex::new(HashMap::new());
}

/// Transforms a [`Cmip6`] object into [`Metadata`]. This is handled differently from the others because CMIP6's path
/// does not contain all the data [`Metadata`] needs. To get the rest we need to either use mapping tables maintained
/// separately from the DRS spec or, as we do here, open the files up and pull the data from their attributes.
async fn metadata_from_cmip6(
    cmip: Cmip6<'_>,
    root_dir: &Utf8Path,
) -> Result<Metadata, ExtractMetadataError> {
    let (frequency, realm) = {
        let mut guard = CMIP6_ATTR_CACHE.lock().await;
        if let Some((freq, realm)) = guard.get(cmip.metadata.table_id) {
            (freq.clone(), realm.clone())
        } else {
            trace!("attributes not cached, opening file to extract");
            let (frequency, realm) = get_cmip6_attrs(&root_dir.join(cmip.path))?;

            guard.insert(
                cmip.metadata.table_id.to_owned(),
                (frequency.clone(), realm.clone()),
            );
            (frequency, realm)
        }
    };

    let m = &cmip.metadata;
    let time_range = m.time_range.as_ref().map(|t| TimeRange {
        start_time: t.start.clone(),
        end_time: t.end.clone(),
    });
    Ok(Metadata {
        path: cmip.path.to_owned(),
        activity: MIP_ERA.to_owned(),
        product: m.activity_id.to_owned(),
        institute: m.institution_id.to_owned(),
        model: m.source_id.to_owned(),
        experiment: m.experiment_id.to_owned(),
        ensemble: m.member_id.variant_label.to_string(),
        variable: m.variable_id.to_owned(),
        version: Some(m.version.to_owned()),
        mip_table: m.table_id.to_owned(),
        modeling_realm: realm,
        frequency,
        time_range,
    })
}

fn get_cmip6_attrs(path: &Utf8Path) -> Result<(String, String), ExtractMetadataError> {
    // this netcdf library has an internal global mutex meaning it doesn't play well with multithreaded code.
    // It's safe but will slow things down. I think this will be mostly ok since data of one type will already be
    // mostly single threaded but the mutex may not play nicely with tokio
    // I'm not sure if sending this into a `spawn_blocking` would help because of the global mutex. I'll leave it as
    // is until it's more clear if not doing so is a problem.
    let f = netcdf::open(path)?;

    let frequency_attr = f
        .attribute("frequency")
        .ok_or_else(|| ExtractMetadataError::MissingAttribute("frequency".to_string()))?
        .value()?;

    let frequency = match frequency_attr {
        AttrValue::Str(s) => s,
        val => {
            return Err(ExtractMetadataError::InvalidAttributeData {
                key: "frequency".to_owned(),
                expected: "str".to_owned(),
                found: attrvalue_to_str(&val).to_owned(),
            })
        }
    };

    let realm_attr = f
        .attribute("realm")
        .ok_or_else(|| ExtractMetadataError::MissingAttribute("realm".to_owned()))?
        .value()?;
    let realm = match realm_attr {
        AttrValue::Str(s) => s,
        val => {
            return Err(ExtractMetadataError::InvalidAttributeData {
                key: "realm".to_owned(),
                expected: "str".to_owned(),
                found: attrvalue_to_str(&val).to_owned(),
            });
        }
    };

    Ok((frequency, realm))
}

/// Gets a printable type name for each attrvalue for error handling.
///
/// Could probably be replaced by `std::any::type_name_of_val` whenever it's stabilized,
/// [tracking issue](https://github.com/rust-lang/rust/issues/66359).
fn attrvalue_to_str(a: &AttrValue) -> &'static str {
    use AttrValue::*;
    match *a {
        Uchar(_) => "u8",
        Uchars(_) => "Vec<u8>",
        Schar(_) => "i8",
        Schars(_) => "Vec<i8>",
        Ushort(_) => "u16",
        Ushorts(_) => "Vec<u16>",
        Short(_) => "i16",
        Shorts(_) => "Vec<i16>",
        Uint(_) => "u32",
        Uints(_) => "Vec<u32>",
        Int(_) => "i32",
        Ints(_) => "Vec<i32>",
        Ulonglong(_) => "u64",
        Ulonglongs(_) => "Vec<u64>",
        Longlong(_) => "i64",
        Longlongs(_) => "Vec<i64>",
        Float(_) => "f32",
        Floats(_) => "Vec<f32>",
        Double(_) => "f64",
        Doubles(_) => "Vec<f64>",
        Str(_) => "str",
        Strs(_) => "Vec<str>",
    }
}

/// Represents the time range covered by the data within the file. Because Solr will valid this data, it is a datetime
/// object and not a string like the rest of the data.
#[derive(Debug, PartialEq, Eq)]
pub struct TimeRange {
    start_time: PartialDateTime,
    end_time: PartialDateTime,
}

impl TimeRange {
    fn to_solr_string(&self) -> String {
        format!(
            "[{} TO {}]",
            partialdatetime_to_solr_str(&self.start_time),
            partialdatetime_to_solr_str(&self.end_time),
        )
    }

    fn fixed() -> Self {
        Self {
            start_time: PartialDateTime::from_y(0),
            end_time: PartialDateTime::from_y(9999),
        }
    }
}

fn partialdatetime_to_solr_str(p: &PartialDateTime) -> String {
    let mut s = format!("{:04}", p.year);
    if let Some(month) = p.month {
        let _ = write!(s, "-{:02}", month);
    } else {
        return s;
    }

    if let Some(day) = p.day {
        let _ = write!(s, "-{:02}", day);
    } else {
        return s;
    }

    if let Some(hour) = p.hour {
        let _ = write!(s, "T{:02}", hour);
    } else {
        return s;
    }

    if let Some(minute) = p.minute {
        let _ = write!(s, ":{:02}", minute);
    } else {
        return s;
    }

    if let Some(second) = p.second {
        let _ = write!(s, ":{:02}", second);
    }

    s
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_time_range() {
        let input = TimeRange {
            start_time: PartialDateTime::from_ymd_hms(1950, 2, 3, 4, 5, 6),
            end_time: PartialDateTime::from_ymd_hms(1951, 12, 11, 10, 9, 8),
        };
        let output = input.to_solr_string();
        assert_eq!("[1950-02-03T04:05:06 TO 1951-12-11T10:09:08]", output);
        let input = TimeRange {
            start_time: PartialDateTime::from_ymd_h(1950, 2, 3, 4),
            end_time: PartialDateTime::from_ymd_h(1951, 12, 11, 10),
        };
        let output = input.to_solr_string();
        assert_eq!("[1950-02-03T04 TO 1951-12-11T10]", output);
    }

    #[test]
    fn test_fixed_time_range() {
        let input = TimeRange::fixed();
        let output = input.to_solr_string();
        assert_eq!("[0000 TO 9999]", output);
    }
}
