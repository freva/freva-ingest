use std::{
    env::current_dir,
    io,
    path::{Component, Path, PathBuf},
};

/// Resolves `.` and `..` in a path to a fully qualified path without resolving symlinks. This also will check if the
/// path is already fully qualified and if not, will prepend the path with the current directory (using
/// [`std::env::current_dir`]).
///
/// The majority of this function is copied from
/// [cargo](https://github.com/rust-lang/cargo/blob/6d6dd9d9be9c91390da620adf43581619c2fa90e/crates/cargo-util/src/paths.rs#L81)
///
/// There is currently an unstable function that does some of what this tries to do which is worth keeping an eye on:
/// <https://github.com/rust-lang/rust/issues/92750>
pub(super) fn absolute(path: &Path) -> Result<PathBuf, io::Error> {
    let path = if !path.has_root() {
        let root = current_dir()?;
        root.join(path)
    } else {
        path.to_owned()
    };

    let mut components = path.components().peekable();
    let mut ret = if let Some(c @ Component::Prefix(..)) = components.peek().cloned() {
        components.next();
        PathBuf::from(c.as_os_str())
    } else {
        PathBuf::new()
    };

    for component in components {
        match component {
            Component::Prefix(..) => unreachable!(),
            Component::RootDir => {
                ret.push(component.as_os_str());
            }
            Component::CurDir => {}
            Component::ParentDir => {
                ret.pop();
            }
            Component::Normal(c) => {
                ret.push(c);
            }
        }
    }
    Ok(ret)
}
