//! Handles querying Solr for files

use std::collections::HashMap;

use thiserror::Error;

use crate::{
    solr::{Solr, SolrError},
    DEFAULT_LATEST_COLLECTION,
};

use super::{Config, Metadata};

#[derive(Debug, Error)]
pub enum Error {
    #[error("error in solr")]
    Solr(#[from] SolrError),

    #[error("error extracting DRS metadata")]
    InvalidFile(#[from] super::Error),
}

/// Searches Solr for drs file metadata.
pub async fn search(drs_conf: &Config, solr: &Solr) -> Result<Vec<Metadata>, Error> {
    let facets = HashMap::new();

    let mut start = 0;
    let mut max = None;
    let mut files = Vec::new();

    while max.is_none() || start < max.unwrap() {
        let resp = solr
            .search(DEFAULT_LATEST_COLLECTION, start, &facets)
            .await?;
        let docs = resp.response.docs;

        for d in docs.iter() {
            println!("{}", d.file);
            let file = drs_conf.metadata_from_path(&d.file).await?;
            files.push(file);
        }
        start += docs.len();
        if max.is_none() {
            max = Some(resp.response.num_found);
        }
        if start >= max.unwrap() {
            break;
        }
    }

    Ok(files)
}
