//! Handles inserting DRS file metadata into Solr

use std::cmp::Ordering;
use std::collections::HashMap;
use std::io;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;

use camino::Utf8PathBuf;
use thiserror::Error;
use tokio::sync::mpsc::{channel, Receiver};
use tokio::time::Instant;
use tracing::{error, info, instrument, trace, warn};
use walkdir::{DirEntry, WalkDir};

use crate::drs::Dataset;
use crate::{
    drs::{Config, Metadata},
    solr::{Solr, SolrError},
};
use crate::{DEFAULT_FILES_COLLECTION, DEFAULT_LATEST_COLLECTION};

use super::path::absolute;

/// Errors possible while ingesting data into solr
#[derive(Debug, Error)]
pub enum IngestError {
    /// Error communicating with or inserting data into Solr
    #[error("error inserting into store")]
    InsertFail(#[from] SolrError),

    /// Error crawling the filesystem tree. Most likely a symlink loop or a broken file. See underlying error for more
    /// details.
    #[error("error while walking the directory structure: {0}")]
    WalkDirectory(#[from] walkdir::Error),

    /// Error canonicalizing data dir path
    #[error("error getting absolute path for data-dir")]
    AbsolutePath(#[from] io::Error),

    /// Given data dir was not contained in any known dataset
    #[error("Given data-dir was not contained in any known dataset")]
    InvalidDataDir,
}

#[derive(Debug)]
pub struct IngestReport {
    pub datasets: Vec<DatasetReport>,
    pub total_time: Duration,
}

#[derive(Debug)]
pub struct DatasetReport {
    pub dataset: Dataset,
    pub sent: u32,
    pub skipped: u32,
    pub time: Duration,
}

/// Ingests metadata from filesystem into freva's solr
///
/// Spawns 2 tasks per [`Dataset`] in the DRS config where `root_dir` is contained in (i.e. `starts_with`) `data_dir`.
/// One task is the computation thread which processes the file names as they come in and it spawns the second task
/// which is a blocking thread that uses `walkdir` synchronously and feeds the paths it finds into a channel.
///
/// `files_collection` and `latest_collection` are optional. If `None`, then this will use Freva's defaults:
/// [`DEFAULT_FILES_COLLECTION`] and [`DEFAULT_LATEST_COLLECTION`], respectively.
pub async fn ingest(
    solr: &Solr,
    drs_conf: &Config,
    data_dir: &Option<&Path>,
    batch_size: usize,
    files_collection: Option<&str>,
    latest_collection: Option<&str>,
) -> Result<IngestReport, IngestError> {
    let mut handles = Vec::new();

    let start_time = Instant::now();

    // immediately canonicalize data-dir if given to ensure we always operate on the fully qualified form
    let data_dir = match data_dir {
        //Some(p) => Some(tokio::fs::canonicalize(p).await?),
        Some(p) => Some(absolute(p)?),
        None => None,
    };

    // If we were not given data-dir, ingest all datasets
    // If we were given a data-dir then look for a dataset that contains it and only ingest from that dataset
    let datasets: Vec<&Dataset> = match data_dir.as_ref() {
        None => drs_conf.datasets().collect(),
        Some(p) => {
            let dataset = drs_conf.datasets().find(|d| p.starts_with(d.root()));
            match dataset {
                None => return Err(IngestError::InvalidDataDir),
                Some(d) => vec![d],
            }
        }
    };

    for dataset in datasets.into_iter() {
        // clone for each thread to capture without worrying about lifetimes
        let solr = solr.clone();
        let dataset = dataset.clone();
        let files_collection = files_collection
            .unwrap_or(DEFAULT_FILES_COLLECTION)
            .to_owned();
        let latest_collection = latest_collection
            .unwrap_or(DEFAULT_LATEST_COLLECTION)
            .to_owned();
        let data_dir = data_dir.as_ref().cloned();
        handles.push(tokio::spawn(async move {
            ingest_dataset(
                solr,
                dataset,
                data_dir,
                batch_size,
                &files_collection,
                &latest_collection,
            )
            .await
        }));
    }

    let results = futures::future::join_all(handles).await;

    let (success, errors): (Vec<_>, Vec<_>) = results.into_iter().partition(|r| r.is_ok());
    for err in errors.into_iter().map(|e| e.unwrap_err()) {
        error!("error while processing a DRS dataset: {}", err);
    }

    let (results, errors): (Vec<_>, Vec<_>) = success
        .into_iter()
        .map(|s| s.unwrap())
        .partition(|r| r.is_ok());

    for err in errors.into_iter().map(|e| e.unwrap_err()) {
        error!("error while processing a DRS dataset: {}", err);
    }

    let reports = results.into_iter().map(|r| r.unwrap()).collect();

    Ok(IngestReport {
        datasets: reports,
        total_time: start_time.elapsed(),
    })
}

/// Ingests a single dataset from the DRS config.
///
/// This handles crawling either the dataset's `root_dir` or the given `ingest_dir` and ingesting any files it finds
/// that are valid for the dataset.  This inserts the files it finds into both files and latest collection within solr.
///
/// This expects that `ingest_dir` will be a fully qualified path if set.
/// Invalid files, whatever the reason they're invalid, will be skipped. Failing to send to solr will result in this
/// ending early with an error. Any files that were already sent to solr will not be cleaned up.
#[instrument(skip_all, fields(dataset = dataset.name(), batch_size=batch_size))]
async fn ingest_dataset(
    solr: Solr,
    dataset: Dataset,
    ingest_dir: Option<PathBuf>,
    batch_size: usize,
    files_collection: &str,
    latest_collection: &str,
) -> Result<DatasetReport, IngestError> {
    let mut file_buf = Vec::with_capacity(batch_size);
    let mut latest_buf = Vec::with_capacity(batch_size);

    let mut sent = 0;
    let mut skipped = 0;
    let mut latest_versions: HashMap<String, String> = HashMap::new();

    let ingest_dir = match ingest_dir {
        Some(p) => p,
        None => dataset.root().as_std_path().to_owned(),
    };
    let walkdir = WalkDir::new(ingest_dir)
        .follow_links(true)
        // this sort_by is _extremely important_. The current way we track the latest version of the file relies on
        // it receiving the latest version as the first of the files with the same unversioned identifier
        .sort_by(|a, b| a.file_name().cmp(b.file_name()).reverse());
    let mut entries = walkdir_channel(walkdir);

    let start_time = Instant::now();

    while let Some(entry) = entries.recv().await {
        if file_buf.len() >= batch_size {
            sent += flush(&solr, &dataset, files_collection, &mut file_buf).await?;
        }
        if latest_buf.len() >= batch_size {
            flush(&solr, &dataset, latest_collection, &mut latest_buf).await?;
        }

        let entry = match entry {
            Ok(entry) => entry,
            Err(e) => {
                // if the file isn't found, something is probably broken with Lustre, skip it and move on.
                // other errors will still result in ingestion ending early
                let path = e.path().unwrap_or_else(|| Path::new("")).display();
                if let Some(inner) = e.io_error() {
                    if inner.kind() == io::ErrorKind::NotFound
                        || inner.kind() == io::ErrorKind::PermissionDenied
                    {
                        warn!("found broken file, skipping: {path}");
                        skipped += 1;
                        continue;
                    }
                }
                return Err(IngestError::WalkDirectory(e));
            }
        };

        if entry.path().is_file() {
            trace!("checking path: '{}'", entry.path().display());
            let path = match Utf8PathBuf::from_path_buf(entry.path().to_path_buf()) {
                Ok(p) => p,
                Err(e) => {
                    warn!(
                        "{} not a valid drs file, has non UTF8 characters in path:\n{:?}",
                        entry.path().display(),
                        e
                    );
                    skipped += 1;
                    continue;
                }
            };

            let f = match dataset.metadata_from_path(&path).await {
                Ok(f) => f,
                Err(e) => {
                    warn!("{} not a valid drs file, skipping:\n{}", path, e);
                    skipped += 1;
                    continue;
                }
            };

            let f = Arc::new(f);
            // given the reverse sorting above, we know the first (versioned) file we see with a new unversioned
            // identifier will be the latest for that particular set of files
            if let Some(version) = f.version() {
                let id = f.to_unversioned_identifier();

                if let Some(latest) = latest_versions.get_mut(&id) {
                    // clippy complained about using if statements here which I think was slightly clearer but not
                    // enough to override it
                    match version.cmp(latest) {
                        Ordering::Greater => {
                            // if this happens that either means the files are not coming in reverse sorted order or
                            // there is a collision in identifiers where 2 different files have the same unversioned ID
                            // but different versions, something which shouldn't happen in a proper dataset given a
                            // reverse sorted ordering
                            let path = f.path();
                            warn!(
                                concat!(
                                    "{path} has a higher version than has already been seen for this identifier, ",
                                    "latest core may not contain the correct files"
                                ),
                                path = path
                            );
                            *latest = version.clone();
                            latest_buf.push(f.clone());
                        }
                        Ordering::Equal => {
                            // we've likely come across a file which is part of the same set of files and differs by a
                            // property which is not tracked in the unversioned ID (currently only time range). These
                            // should be added to the latest core as separate files.
                            //
                            // e.g.
                            // foo/bar/v1/whatever_2003-2004.nc
                            // foo/bar/v1/whatever_2001-2002.nc
                            // these would have the same unversioned ID and have the same version but are valid because
                            // they cover different time ranges which isn't tracked in the dataset identifier we're
                            // using

                            latest_buf.push(f.clone());
                        }
                        Ordering::Less => {
                            // this is the average case where the file's ID has already been seen and its version is
                            // lower than the latest so we don't need to add it to the latest core hence nothing is
                            // done here
                        }
                    }
                } else {
                    trace!("found new unversioned id {id}, submitting version {version} to latest");
                    latest_versions.insert(id, version.clone());
                    latest_buf.push(f.clone());
                }
            } else {
                // not versioned, always add to latest
                latest_buf.push(f.clone());
            }

            file_buf.push(f.clone());
        }
    }

    if !file_buf.is_empty() {
        sent += flush(&solr, &dataset, files_collection, &mut file_buf).await?;
    }
    if !latest_buf.is_empty() {
        flush(&solr, &dataset, latest_collection, &mut latest_buf).await?;
    }

    Ok(DatasetReport {
        dataset,
        sent,
        skipped,
        time: start_time.elapsed(),
    })
}

#[instrument(skip_all, fields(collection=collection))]
async fn flush(
    solr: &Solr,
    dataset: &Dataset,
    collection: &str,
    docs: &mut Vec<Arc<Metadata>>,
) -> Result<u32, IngestError> {
    info!(
        "flushing {} documents in buffer to collection {}",
        docs.len(),
        collection
    );
    let sent = docs.len() as u32;
    // turn Vec<Rc<_>> in Vec<&_>
    let doc_refs: Vec<_> = docs
        .iter()
        .map(|d| {
            let mut m = d.to_solr_map();
            // inject the name of the dataset to the final map
            m.insert("dataset", dataset.name().to_owned());
            m
        })
        .collect();
    solr.update(collection, &doc_refs).await?;
    docs.clear();
    Ok(sent)
}

/// Creates a channel for offloading filesystem access onto a separate task.
///
/// This is necessary because the filesystem can be extremely slow when dealing with large numbers of files but walkdir
/// doesn't provide an async API that would be more appropriate. Instead we throw it into a separate task which goes
/// into tokio's blocking pool and should result in less contention.
fn walkdir_channel(walkdir: WalkDir) -> Receiver<walkdir::Result<DirEntry>> {
    let (tx, rx) = channel(100);
    tokio::task::spawn_blocking(move || {
        for entry in walkdir {
            if let Err(e) = tx.blocking_send(entry) {
                error!("{}", e);
                return;
            }
        }
    });
    rx
}
