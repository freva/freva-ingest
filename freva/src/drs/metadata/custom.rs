//! This is an escape hatch for files/datasets that don't have a path or name that conforms to an implemented DRS
//! specification. This will require more configuration in the config file to describe how to turn the path and name
//! into all the necessary information for Freva.
//!
//! This requires the creation of a [`ComponentConfig`] to describe where in the path or file name certain values are.
//! Component names aside from the required ones are allowed for clarity when setting up or changing the config but
//! will be ignored when converted to a [`Custom`] object.

use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};

use camino::Utf8Path;
use drs::cmip5::{InvalidPartialDateTime, PartialDateTime};
use lazy_static::lazy_static;
use thiserror::Error;

use super::TimeRange;

/// Possible errors when extracting metadata from a path
#[derive(Debug, Error)]
pub enum Error {
    /// The path did not have the same number of elements as there are in [`ComponentConfig::parts_dir`]
    #[error("incorrect number of path parts: has {0}, expected {1}")]
    MismatchPathParts(usize, usize),
    /// The file name did not have the same number of elements as there are in [`ComponentConfig::parts_file_name`]
    #[error("incorrect number of file name parts: has {0}, expected {1}")]
    MismatchFilenameParts(usize, usize),
    /// The path did not have a file name
    #[error("no file name found, this may be a directory")]
    NoFilename,
    /// The path was missing some required values
    #[error("missing required values: needs {0:?}")]
    MissingValues(Vec<String>),
    /// `time_range` was included but was not in a valid format.
    #[error("time_range value was not in valid format: {0}")]
    InvalidTimeRange(#[from] ParseTimeRangeError),
}

// define all the used keys here to avoid duplicating the values everywhere. The names reflect their counterparts in
// the struct and not the values that are read from the config
const KEY_ACTIVITY: &str = "project";
const KEY_PRODUCT: &str = "product";
const KEY_INSTITUTE: &str = "institute";
const KEY_MODEL: &str = "model";
const KEY_EXPERIMENT: &str = "experiment";
const KEY_FREQUENCY: &str = "time_frequency";
const KEY_MODELING_REALM: &str = "realm";
const KEY_MIP_TABLE: &str = "cmor_table";
const KEY_ENSEMBLE: &str = "ensemble";
const KEY_VARIABLE: &str = "variable";
const KEY_VERSION: &str = "version";
const KEY_TIME_RANGE: &str = "time";

lazy_static! {
    // these are different from the struct field names for backwards compatibility. The struct field names match the
    // cmip5 spec they're derived from but these are the names that were previously used in Freva
    static ref REQUIRED: HashSet<&'static str> = HashSet::from([
        KEY_ACTIVITY,
        KEY_PRODUCT,
        KEY_INSTITUTE,
        KEY_MODEL,
        KEY_EXPERIMENT,
        KEY_FREQUENCY,
        KEY_MODELING_REALM,
        KEY_MIP_TABLE,
        KEY_ENSEMBLE,
        KEY_VARIABLE,
    ]);
}

/// Config which describes a custom DRS-like file naming scheme to ingest into Freva's metadata format.
///
/// Expected part names are:
/// ```text
/// activity
/// product
/// institute
/// model
/// experiment
/// modeling_realm
/// mip_table
/// ensemble
/// variable
/// ```
///
/// The following names are optional and will only be looked for if present in the config:
/// ```text
/// version
/// time
/// ```
///
/// Parts can be located in either the path (`/` delineated) or in the filename (`_` delineated). They can also be
/// pressent multiple times in both path and filename, if that is the case, the last instance will be the final value
/// and all previous ones are discarded. This mapping is very simple so more advanced processing like splitting or
/// joining sections is not supported.
///
/// All part names are accepted directly as strings and will have no verification for correctness or conformity to any
/// existing DRS specification in terms of allowed values. Additional names are also allowed but are ignored. This can
/// be used to mark out parts of the path/name that aren't used for DRS data.
#[derive(Debug, Clone)]
pub struct ComponentConfig {
    /// The keys of the different path segments in the order they are expected to appear in the path. This does not
    /// include the file name which is always at the end so no `filename` part is necessary.
    pub parts_dir: Vec<String>,
    /// Key ordering from parts of the file name
    pub parts_file_name: Vec<String>,
    /// Default values used to fill in any missing values for the equivalent key
    pub defaults: HashMap<String, String>,
}

impl ComponentConfig {
    pub(crate) fn metadata_from_path<'path, 'config: 'path>(
        &'config self,
        path: &'path Utf8Path,
    ) -> Result<Custom<'path>, Error> {
        let parts: Vec<&str> = path.iter().collect();
        // + 1 because parts_dir does not include the filename
        if parts.len() != self.parts_dir.len() + 1 {
            return Err(Error::MismatchPathParts(parts.len(), self.parts_dir.len()));
        }
        // remove file extension if present
        let filename = path.file_stem().ok_or(Error::NoFilename)?;

        let filename_parts: Vec<&str> = filename.split('_').collect();
        if filename_parts.len() != self.parts_file_name.len() {
            return Err(Error::MismatchFilenameParts(
                filename_parts.len(),
                self.parts_file_name.len(),
            ));
        }

        let mut components = HashMap::new();
        // defaults first so they're overwritten if present
        for (name, value) in self.defaults.iter() {
            components.insert(name.as_str(), value.as_ref());
        }

        for (name, value) in self.parts_dir.iter().zip(parts) {
            components.insert(name.as_str(), value);
        }

        for (name, value) in self.parts_file_name.iter().zip(filename_parts) {
            components.insert(name.as_str(), value);
        }

        Custom::new(path, components)
    }
}

/// Filepath that conforms to the corresponding [`ComponentConfig`].
///
/// This is essentially the same as [`super::Metadata`] except the data isn't owned to conform more to the convention set
/// by the other DRS specs.
///
/// Paths must be UTF-8.
#[derive(Debug, PartialEq, Eq)]
pub struct Custom<'a> {
    pub(crate) path: &'a Utf8Path,
    pub(crate) activity: &'a str,
    pub(crate) product: &'a str,
    pub(crate) institute: &'a str,
    pub(crate) model: &'a str,
    pub(crate) experiment: &'a str,
    pub(crate) frequency: &'a str,
    pub(crate) modeling_realm: &'a str,
    pub(crate) variable: &'a str,
    pub(crate) ensemble: &'a str,
    pub(crate) mip_table: &'a str,
    pub(crate) version: Option<&'a str>,
    pub(crate) time_range: Option<TimeRange>,
}

impl<'a> Custom<'a> {
    fn new(path: &'a Utf8Path, components: HashMap<&str, &'a str>) -> Result<Self, Error> {
        let key_set = components.keys().copied().collect();

        let missing: Vec<_> = REQUIRED
            .difference(&key_set)
            .map(|&m| m.to_owned())
            .collect();
        if !missing.is_empty() {
            return Err(Error::MissingValues(Vec::from_iter(missing)));
        }

        let time_range = match components.get(KEY_TIME_RANGE) {
            Some(r) => Some(parse_time_range(r)?),
            None => None,
        };

        Ok(Self {
            path,
            activity: components[KEY_ACTIVITY],
            product: components[KEY_PRODUCT],
            institute: components[KEY_INSTITUTE],
            model: components[KEY_MODEL],
            experiment: components[KEY_EXPERIMENT],
            frequency: components[KEY_FREQUENCY],
            modeling_realm: components[KEY_MODELING_REALM],
            variable: components[KEY_VARIABLE],
            ensemble: components[KEY_ENSEMBLE],
            mip_table: components[KEY_MIP_TABLE],
            version: components.get(KEY_VERSION).copied(),
            time_range,
        })
    }
}

/// Error during parsing of a time range in a custom DRS dataset.
#[derive(Debug, Error)]
#[error("unable to parse given string as time range")]
pub enum ParseTimeRangeError {
    /// One or both of the time strings was not in a valid form
    InvalidDate(#[from] InvalidPartialDateTime),
    /// The time_range value should be of the form `start-end`. If there are more or fewer sections when split on `-`,
    /// this will be considered invalid.
    #[error("incorrect number of date sections: has {0}, expected {1}")]
    MismatchDateParts(usize, usize),
}

fn parse_time_range(i: &str) -> Result<TimeRange, ParseTimeRangeError> {
    let parts: Vec<&str> = i.split('-').collect();
    if parts.len() != 2 {
        return Err(ParseTimeRangeError::MismatchDateParts(parts.len(), 2));
    }
    let start = PartialDateTime::from_str(parts[0])?;
    let end = PartialDateTime::from_str(parts[1])?;
    Ok(TimeRange {
        start_time: start,
        end_time: end,
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    fn owned_vec(v: Vec<&str>) -> Vec<String> {
        v.into_iter().map(|s| s.to_owned()).collect()
    }

    #[test]
    fn test_from_normal_path() {
        let path = Utf8Path::new("act/prod/inst/mod/exp/freq/v1/var_modrel_mip_ens.nc");
        let config = ComponentConfig {
            parts_dir: owned_vec(vec![
                KEY_ACTIVITY,
                KEY_PRODUCT,
                KEY_INSTITUTE,
                KEY_MODEL,
                KEY_EXPERIMENT,
                KEY_FREQUENCY,
                KEY_VERSION,
            ]),
            parts_file_name: owned_vec(vec![
                KEY_VARIABLE,
                KEY_MODELING_REALM,
                KEY_MIP_TABLE,
                KEY_ENSEMBLE,
            ]),
            defaults: HashMap::new(),
        };

        let custom = config.metadata_from_path(path).unwrap();
        assert_eq!(
            Custom {
                path,
                activity: "act",
                product: "prod",
                institute: "inst",
                model: "mod",
                experiment: "exp",
                frequency: "freq",
                modeling_realm: "modrel",
                variable: "var",
                ensemble: "ens",
                mip_table: "mip",
                version: Some("v1"),
                time_range: None,
            },
            custom
        );
    }

    #[test]
    fn test_optional_version() {
        let path = Utf8Path::new("act/prod/inst/mod/exp/freq/var_modrel_mip_ens.nc");
        let config = ComponentConfig {
            parts_dir: owned_vec(vec![
                KEY_ACTIVITY,
                KEY_PRODUCT,
                KEY_INSTITUTE,
                KEY_MODEL,
                KEY_EXPERIMENT,
                KEY_FREQUENCY,
            ]),
            parts_file_name: owned_vec(vec![
                KEY_VARIABLE,
                KEY_MODELING_REALM,
                KEY_MIP_TABLE,
                KEY_ENSEMBLE,
            ]),
            defaults: HashMap::new(),
        };

        let custom = config.metadata_from_path(path).unwrap();
        assert_eq!(
            Custom {
                path,
                activity: "act",
                product: "prod",
                institute: "inst",
                model: "mod",
                experiment: "exp",
                frequency: "freq",
                modeling_realm: "modrel",
                variable: "var",
                ensemble: "ens",
                mip_table: "mip",
                version: None,
                time_range: None,
            },
            custom
        );
    }
}
