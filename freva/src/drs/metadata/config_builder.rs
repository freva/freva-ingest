//! This handles the conversion between the external DRS config format and the internal representation we use while
//! working the filesystem.

use std::collections::HashMap;

use camino::Utf8PathBuf;
use serde::Deserialize;
use thiserror::Error;

use crate::drs::path::absolute;

use super::{custom::ComponentConfig, Config, Dataset, Specification};

/// Deserializable builder for a [`Config`]. Allows the config file to be simpler and friendlier without forcing our
/// internal representation to be identical.
#[derive(Debug, Deserialize)]
pub struct ConfigBuilder {
    #[serde(flatten)]
    inner: HashMap<String, DatasetBuilder>,
}

/// Contains all the elements necessary to create a dataset. Most will have just `root_dir` and `drs_format` but a
/// custom format would require some of the additional elements.
#[derive(Debug, Deserialize)]
struct DatasetBuilder {
    root_dir: Utf8PathBuf,
    drs_format: Format,
    #[serde(default)]
    parts_dir: Option<Vec<String>>,
    #[serde(default)]
    parts_file_name: Option<Vec<String>>,
    #[serde(default)]
    defaults: Option<HashMap<String, String>>,
}

/// Like [`Specification`] but without additional information in `Custom` since at when we're building the config we
/// won't know we need it yet.
#[derive(Debug, Deserialize)]
enum Format {
    #[serde(rename = "cmor")]
    Cmor,
    #[serde(rename = "cmip5")]
    Cmip5,
    #[serde(rename = "cmip6")]
    Cmip6,
    #[serde(rename = "cordex")]
    Cordex,
    #[serde(rename = "custom")]
    Custom,
}

#[derive(Debug, Error)]
pub enum BuildError {
    #[error("`{0}` value is missing for custom format")]
    MissingRequiredField(String),
    #[error("error converting relative root_dir to fully qualified: {0}")]
    AbsolutePath(#[from] std::io::Error),
    #[error("root_dir is not valid UTF8")]
    VerifyPathUtf8(#[from] camino::FromPathBufError),
}

impl ConfigBuilder {
    /// Builds and verifies a [`Config`] from the information in `self`.
    pub fn build(self) -> Result<Config, BuildError> {
        let mut datasets: Vec<Dataset> = Vec::with_capacity(self.inner.len());
        for (name, s) in self.inner.into_iter() {
            let dataset = s.build(name)?;
            datasets.push(dataset);
        }
        Ok(Config { datasets })
    }
}

impl DatasetBuilder {
    fn build(self, name: String) -> Result<Dataset, BuildError> {
        use Format::*;
        let drs_format = match self.drs_format {
            Cmip5 => Specification::Cmip5,
            Cmor => Specification::Cmor,
            Cmip6 => Specification::Cmip6,
            Cordex => Specification::Cordex,
            Custom => {
                let conf = self.build_component_config()?;
                Specification::Custom(conf)
            }
        };

        // fully qualify the path name and expand things like `~` then convert back to a `Utf8PathBuf`
        let root_dir = absolute(self.root_dir.as_std_path())?;
        let root_dir = Utf8PathBuf::try_from(root_dir)?;

        Ok(Dataset {
            name,
            root_dir,
            drs_format,
        })
    }

    fn build_component_config(&self) -> Result<ComponentConfig, BuildError> {
        // This is cloning all the fields which I don't love but taking ownership would be a problem for `build`.
        // This is only done a handful of times anyway so it should be ok.
        let parts_dir = self
            .parts_dir
            .clone()
            .ok_or_else(|| BuildError::MissingRequiredField("parts_dir".to_owned()))?;

        let parts_file_name = self
            .parts_file_name
            .clone()
            .ok_or_else(|| BuildError::MissingRequiredField("parts_file_name".to_owned()))?;

        let defaults = match &self.defaults {
            Some(m) => m.clone(),
            None => HashMap::new(),
        };

        Ok(ComponentConfig {
            parts_dir,
            parts_file_name,
            defaults,
        })
    }
}
