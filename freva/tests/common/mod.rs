use freva::drs::metadata::ConfigBuilder;
use freva::drs::Config;
use freva::solr::Solr;
use std::{collections::HashMap, sync::Once};
use wiremock::MockServer;

static INIT: Once = Once::new();

pub const REPO_ROOT: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/..");

pub fn log_init() {
    // ensure that this is only run once, running multiple integration tests in a batch will result in all but the first
    // crashing.
    // individual tests still must call this so if they are run separately, they will initialize the logger
    INIT.call_once(|| {
        tracing_subscriber::fmt::init();
    });
}

pub async fn solr_server() -> (MockServer, Solr) {
    let server = MockServer::start().await;

    let url = reqwest::Url::parse(&server.uri()).unwrap();
    let domain = url.host_str().unwrap().to_owned();
    let port = url.port();

    let solr = Solr::new(domain, port, None);
    assert!(solr.is_ok(), "invalid solr address");
    (server, solr.unwrap())
}

pub fn test_config() -> Config {
    let string = format!(
        r#"
            [observations]
                root_dir = "{REPO_ROOT}/.docker/data/obs"
                drs_format = "custom"
                parts_dir = [
                    "project", "product", "institute", "model", "experiment", "time_frequency", "realm",
                    "cmor_table", "ensemble", "version", "variable"
                ]
                parts_file_name = ["variable", "time_frequency", "experiment", "level", "version", "time"]
                parts_time = "start_time-end_time"
                [observations.defaults]
                    project = "observations"
            [cmip5_name]
                root_dir = "{REPO_ROOT}/.docker/data/cmip5"
                drs_format = "cmip5"
            [cmip6_name]
                root_dir = "{REPO_ROOT}/.docker/data/cmip6"
                drs_format = "cmip6"
            "#,
    );
    let builder: ConfigBuilder = toml::from_str(&string).unwrap();
    builder.build().unwrap()
}

pub fn owned_string_map(m: &[(&str, &str)]) -> HashMap<String, String> {
    m.iter()
        .map(|(k, v)| (k.to_string(), v.to_string()))
        .collect()
}
