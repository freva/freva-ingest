use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

use camino::Utf8PathBuf;
use wiremock::{
    matchers::{method, path},
    Mock, Request, ResponseTemplate,
};

use crate::common::owned_string_map;

mod common;

#[tokio::test]
async fn test_ingest() {
    common::log_init();
    let (mock, solr) = common::solr_server().await;
    Mock::given(method("POST"))
        .and(path("/solr/files/update/json/docs"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&mock)
        .await;
    Mock::given(method("POST"))
        .and(path("/solr/latest/update/json/docs"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&mock)
        .await;

    let drs_config = common::test_config();
    let data_dir: Option<&Path> = None;

    let result = freva::drs::ingest(&solr, &drs_config, &data_dir, 10, None, None).await;
    assert!(result.is_ok());
    let report = result.unwrap();
    let expected_results: HashMap<&str, (u32, u32)> = HashMap::from_iter(
        [
            ("observations", (24, 0)),
            ("cmip5_name", (13, 0)),
            ("cmip6_name", (6, 0)),
        ]
        .into_iter(),
    );

    assert_eq!(
        expected_results.len(),
        report.datasets.len(),
        "report did not have expected number of dataset reports"
    );
    for dataset_report in report.datasets.iter() {
        let name = dataset_report.dataset.name();
        let (sent, skipped) = expected_results.get(name).expect("unexpected dataset name");
        // can't assert_eq the whole struct because it includes a duration which is non-deterministic
        assert_eq!(
            *sent, dataset_report.sent,
            "unexpected sent number for dataset {}",
            name
        );
        assert_eq!(
            *skipped, dataset_report.skipped,
            "unexpected skipped number for dataset {}",
            name
        );
    }
}

#[tokio::test]
async fn test_ingest_subset() {
    common::log_init();
    let (mock, solr) = common::solr_server().await;
    Mock::given(method("POST"))
        .and(path("/solr/files/update/json/docs"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&mock)
        .await;
    Mock::given(method("POST"))
        .and(path("/solr/latest/update/json/docs"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&mock)
        .await;

    let config_dir = Utf8PathBuf::from(common::REPO_ROOT).join(".docker");
    let data_dir = config_dir.join("data/cmip5/cmip5/output2/INM");
    let drs_config = common::test_config();

    let result = freva::drs::ingest(
        &solr,
        &drs_config,
        &Some(data_dir.as_std_path()),
        10,
        None,
        None,
    )
    .await;
    assert!(result.is_ok());
    let report = result.unwrap();

    assert_eq!(
        1,
        report.datasets.len(),
        "unexpected number of dataset reports"
    );

    let cmip5_results = &report.datasets[0];
    assert_eq!(7, cmip5_results.sent);
}

#[tokio::test]
async fn test_latest_version() {
    common::log_init();
    let config_dir = Utf8PathBuf::try_from(
        PathBuf::from(common::REPO_ROOT)
            .join(".docker")
            .canonicalize()
            .expect("error canonicalizing config dir"),
    )
    .expect("config dir contains non utf8 character");
    let data_dir =
        config_dir.join("data/cmip5/cmip5/output2/INM/inmcm4/esmrcp85/mon/land/Lmon/r1i1p1");
    let drs_config = common::test_config();

    // this is organized in the order they will be sent to solr (i.e. reverse lexicographical order by the file key)
    // both the matchers rely on this to be the case
    let expected = vec![
        owned_string_map(&[
            ("file", data_dir.join("v20110323/residualFrac/residualFrac_Lmon_inmcm4_esmrcp85_r1i1p1_200601-210012.nc").as_str()),
            ("dataset", "cmip5_name"),
            ("product", "output2"),
            ("project", "cmip5"),
            ("institute", "INM"),
            ("model", "inmcm4"),
            ("experiment", "esmrcp85"),
            ("time_frequency", "mon"),
            ("realm", "land"),
            ("ensemble", "r1i1p1"),
            ("cmor_table", "Lmon"),
            ("variable", "residualFrac"),
            ("version", "v20110323"),
            ("time", "[2006-01 TO 2100-12]"),
        ]),
        owned_string_map(&[
            ("file", data_dir.join("v20110323/residualFrac/residualFrac_Lmon_inmcm4_esmrcp85_r1i1p1_200106-200406.nc").as_str()),
            ("dataset", "cmip5_name"),
            ("product", "output2"),
            ("project", "cmip5"),
            ("institute", "INM"),
            ("model", "inmcm4"),
            ("experiment", "esmrcp85"),
            ("time_frequency", "mon"),
            ("realm", "land"),
            ("ensemble", "r1i1p1"),
            ("cmor_table", "Lmon"),
            ("variable", "residualFrac"),
            ("version", "v20110323"),
            ("time", "[2001-06 TO 2004-06]"),
        ]),
        owned_string_map(&[
            ("file", data_dir.join("v20100323/residualFrac/residualFrac_Lmon_inmcm4_esmrcp85_r1i1p1_200601-210012.nc").as_str()),
            ("dataset", "cmip5_name"),
            ("product", "output2"),
            ("project", "cmip5"),
            ("institute", "INM"),
            ("model", "inmcm4"),
            ("experiment", "esmrcp85"),
            ("time_frequency", "mon"),
            ("realm", "land"),
            ("ensemble", "r1i1p1"),
            ("cmor_table", "Lmon"),
            ("variable", "residualFrac"),
            ("version", "v20100323"),
            ("time", "[2006-01 TO 2100-12]"),
        ]),
        owned_string_map(&[
            ("file", data_dir.join("v20100323/residualFrac/residualFrac_Lmon_inmcm4_esmrcp85_r1i1p1_200106-200406.nc").as_str()),
            ("dataset", "cmip5_name"),
            ("product", "output2"),
            ("project", "cmip5"),
            ("institute", "INM"),
            ("model", "inmcm4"),
            ("experiment", "esmrcp85"),
            ("time_frequency", "mon"),
            ("realm", "land"),
            ("ensemble", "r1i1p1"),
            ("cmor_table", "Lmon"),
            ("variable", "residualFrac"),
            ("version", "v20100323"),
            ("time", "[2001-06 TO 2004-06]"),
        ]),
    ];

    // these custom matcher functions are required because the default json matchers will match the json string that's
    // received and hashmaps don't iterate in a predictable order so can't be relied on to produce the exact same string
    // every run. Instead we deserialize them back into hashmaps which have order independent equality

    // clone for the closure
    let expected_match_clone = expected.clone();
    let files_matcher = move |request: &Request| {
        let actual: Vec<HashMap<String, String>> = serde_json::from_slice(&request.body).expect("");
        actual == expected_match_clone
    };

    let latest_expected = expected[0..2].to_vec();
    let latest_matcher = move |request: &Request| {
        let actual: Vec<HashMap<String, String>> = serde_json::from_slice(&request.body).expect("");
        actual == latest_expected
    };

    let (mock, solr) = common::solr_server().await;
    Mock::given(method("POST"))
        .and(path("/solr/files/update/json/docs"))
        .and(files_matcher)
        .respond_with(ResponseTemplate::new(200))
        .mount(&mock)
        .await;
    Mock::given(method("POST"))
        .and(path("/solr/latest/update/json/docs"))
        .and(latest_matcher)
        .respond_with(ResponseTemplate::new(200))
        .mount(&mock)
        .await;

    let result = freva::drs::ingest(
        &solr,
        &drs_config,
        &Some(data_dir.as_std_path()),
        10,
        None,
        None,
    )
    .await;
    assert!(result.is_ok());
    let report = result.unwrap();

    assert_eq!(
        1,
        report.datasets.len(),
        "unexpected number of dataset reports"
    );

    let cmip5_results = &report.datasets[0];
    assert_eq!(4, cmip5_results.sent);
}
