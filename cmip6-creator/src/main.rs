//! Small utility that exists to create cmip6 test files for our integration tests. This will not create full cmip6
//! files with data, they will include just the attributes necessary for ingestion. This is meant just for internal use
//! with this project and not for anything more general.

use std::{fs, path::Path};

use anyhow::Result;

pub const DATA_ROOT: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/../.docker/data/cmip6");

fn main() -> Result<()> {
    let root = Path::new(DATA_ROOT);
    for (path, name, frequency, realm) in data().into_iter() {
        let dir = root.join(path);
        fs::create_dir_all(&dir)?;
        let mut nc = netcdf::create(dir.join(name))?;
        nc.add_attribute("frequency", frequency)?;
        nc.add_attribute("realm", realm)?;
    }

    Ok(())
}

fn data() -> Vec<(&'static str, &'static str, &'static str, &'static str)> {
    // Pulling some of the examples from the cmip6 parser tests
    vec![
        (
            "CMIP6/CMIP/HAMMOZ-Consortium/MPI-ESM-1-2-HAM/1pctCO2/r1i1p1f1/Lmon/tsl/gn/v20190628/",
            "tsl_Lmon_MPI-ESM-1-2-HAM_1pctCO2_r1i1p1f1_gn_191001-192912.nc",
            "mon",
            "land",
        ),
        (
            "CMIP6/AerChemMIP/BCC/BCC-ESM1/hist-piAer/r1i1p1f1/AERmon/c2h6/gn/v20200511/",
            "c2h6_AERmon_BCC-ESM1_hist-piAer_r1i1p1f1_gn_185001-201412.nc",
            "mon",
            "aerosol",
        ),
        (
            "CMIP6/GeoMIP/CCCma/CanESM5/G1/r3i1p2f1/Ofx/areacello/gn/v20190429/",
            "areacello_Ofx_CanESM5_G1_r3i1p2f1_gn.nc",
            "fx",
            "ocean",
        ),
        (
            "CMIP6/HighResMIP/NOAA-GFDL/GFDL-CM4C192/highresSST-future/r1i1p1f1/6hrPlevPt/psl/gr3/v20180701/",
            "psl_6hrPlevPt_GFDL-CM4C192_highresSST-future_r1i1p1f1_gr3_203501010600-205101010000.nc",
            "6hrPt",
            "atmos",
        ),
        (
            "CMIP6/ScenarioMIP/UA/MCM-UA-1-0/ssp370/r1i1p1f2/SImon/sithick/gn/v20190731/",
            "sithick_SImon_MCM-UA-1-0_ssp370_r1i1p1f2_gn_201501-210012.nc",
            "mon",
            "seaice",
        ),
        (
            "CMIP6/CDRMIP/NCAR/CESM2/1pctCO2-cdr/r1i1p1f1/Omon/intpn2/gr/v20190613/",
            "intpn2_Omon_CESM2_1pctCO2-cdr_r1i1p1f1_gr_005001-009912.nc",
            "mon",
            "ocnBgchem",
        ),
    ]
}
